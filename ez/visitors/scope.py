from ez.models.arrays import *
from ez.models.base import *
from ez.models.compound_statements import *
from ez.models.expressions import *
from ez.models.functions import *
from ez.models.literals import *
from ez.models.simple_statements import *
from ez.models.functions import FunctionDefinition
from ez.util import visit as v
from ez.visitors.base import ParentFirstVisitor


class ScopeVisitor(ParentFirstVisitor):

    def __init__(self):
        super(ScopeVisitor, self).__init__()

        self.current_scope_name = None
        self.current_scope_args_name = []

        self.uid_instances = {}

    @v.on('node')
    def visit(self, node):
        '''This is the generic method that initializes the dynamic dispatcher.
        '''

    @v.on('parent')
    def will_accept_children(self, parent):
        pass

    @v.on('parent')
    def did_accept_children(self, parent):
        pass

    @v.when(FunctionDefinition)
    def will_accept_children(self, func_def):
        if func_def.name == 'main':
            self.current_scope_name = None
        else:
            self.current_scope_name = func_def.name

        self.current_scope_args_name = [arg.name for arg in func_def.args]

    @v.when(FunctionDefinition)
    def did_accept_children(self, parent):
        self.current_scope_name = None
        self.current_scope_args_name = []

    # -------------------------------------------------------------------------
    #                              BASE
    # -------------------------------------------------------------------------

    @v.when(ASTObj)
    def visit(self, node):
        ''' ASTObj '''
        pass

    @v.when(Program)
    def visit(self, program):
        ''' Program '''

        main_scope_visitor = ScopeVisitor()
        program.main_function.accept(main_scope_visitor)
        self.uid_instances = main_scope_visitor.uid_instances

    @v.when(Identifier)
    def visit(self, ident):
        ''' Identifier '''

        if ident.name in self.current_scope_args_name or \
                ident.name not in self.uid_instances:
            ident.scope_name = self.current_scope_name

        uid = self.uid_instances.get(ident.scoped_name())

        if uid is None:
            uid = UniqueIdentifier(ident.source_range)
            self.uid_instances[ident.scoped_name()] = uid

        ident.uid = uid
