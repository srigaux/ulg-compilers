from ez.models.base import *
from ez.models.arrays import *
from ez.models.compound_statements import *
from ez.models.expressions import *
from ez.models.functions import *
from ez.models.literals import *
from ez.models.simple_statements import *

from ez.tokenizer.rules import tokens

def p_program(p):
    '''program : statement_plus EOF
    '''

    p[0] = Program(p, p[1])

def p_program_error(p):
    '''program : FOR error empty
               | BREAK error empty
               | CONTINUE error empty
               | IF error empty
    '''
    p.parser.add_error(p, 0, 'Bad statement')


def p_atom(p):
    '''atom : identifier
            | literal
            | LPAREN expression RPAREN
    '''

    if len(p) == 2:
        p[0] = p[1]
    else:
        p[0] = p[2]


def p_identifier(p):
    '''identifier : ID'''

    p[0] = Identifier(p, name=p[1])


def p_atom_rparen_missing_error(p):
    '''atom : LPAREN expression error empty
    '''

    p.parser.add_error(p, 0, 'Right parenthesis is missing')

    p[0] = p[2]


# def p_atom_rparen_error(p):
#     '''atom : ID RPAREN
#             | literal RPAREN
#     '''

#     p.parser.add_error(p, 2, 'Right parenthesis not expected')

#     p[0] = p[1]

def p_literal(p):
    '''literal : INTEGER
               | FLOAT
               | boolean
    '''
    if isinstance(p[1], int):
        p[0] = Integer(p, p[1])
    elif isinstance(p[1], float):
        p[0] = Float(p, p[1])
    else:
        p[0] = p[1]


def p_boolean(p):
    '''boolean : YES
               | NO
    '''
    if p[1] == 'Yes':
        p[0] = Boolean(p, 1)
    else:
        p[0] = Boolean(p, 0)


def p_primary(p):
    '''primary : atom
               | subscription
               | call
    '''
    p[0] = p[1]


def p_subscription(p):
    '''subscription : primary LSQUARE expression RSQUARE
    '''
    p[0] = ArrayAccess(p, p[1], p[3])


def p_subscription_error(p):
    '''subscription : primary LSQUARE error RSQUARE
    '''
    p.parser.add_error(p, 0, "Bad expression in array subscription")
    p[0] = ArrayAccess(p, p[1], Integer(p, 0))


def p_call(p):
    '''call : LSQUARE callparam_list RSQUARE
            | LSQUARE ID RSQUARE
    '''

    name = ''
    args = []

    if isinstance(p[2], str):
        name = p[2]

        if name == 'main':
            name = '$main'

    else:
        for param in p[2]:
            name += param.description + ':'
            args.append(param.value)

    p[0] = FunctionCall(p, name, args)


def p_call_error(p):
    '''call : LSQUARE error empty
    '''
    p.parser.add_error(p, 0, "Bad function call")
    p[0] = Void(p)


# ---------------- Power operator ---------------


def p_power(p):
    '''power : primary
             | primary EXP u_expr
    '''
    if len(p) == 2:
        p[0] = p[1]
    else:
        p[0] = Power(p, p[1], p[3])


def p_power_error(p):
    '''power : primary EXP error empty
    '''
    p.parser.add_error(p, 0, 'Bad expression in exponentiation')
    p[0] = p[1]


# ---------------- Unary expressions ---------------


def p_u_expr(p):
    '''u_expr : power
              | MINUS u_expr
              | PLUS u_expr
    '''
    if len(p) == 2:
        p[0] = p[1]
    else:
        if p[1] == '+':
            p[0] = p[2]
        else:
            p[0] = UnaryExpr(p, p[1], p[2])


def p_u_expr_error(p):
    '''power : MINUS error empty
             | PLUS error empty
    '''
    p.parser.add_error(p, 0, 'Bad expression in unary' + p[1] + ' expression ')
    p[0] = Integer(p, 0)

# ---------------- Multiplicative expressions ---------------


def p_m_expr(p):
    '''m_expr : u_expr
              | m_expr TIMES u_expr
              | m_expr DIVINT u_expr
              | m_expr DIVIDE u_expr
              | m_expr MOD u_expr
    '''
    if len(p) == 2:
        p[0] = p[1]
    else:
        try:
            p[0] = BinaryExpr(p, p[1], p[2], p[3])

        except Exception, e:

            p.parser.add_error(p, 2, e.message)

            p[0] = Integer(p, 0)

# ---------------- Additive expressions ---------------


def p_a_expr(p):
    '''a_expr : m_expr
              | a_expr PLUS m_expr
              | a_expr MINUS m_expr
    '''

    if len(p) == 2:
        p[0] = p[1]
    else:
        p[0] = p[0] = BinaryExpr(p, p[1], p[2], p[3])


# ---------------- Comparisons  -----------------


def p_comparison(p):
    '''comparison : a_expr comparison_r_multi
    '''
    if len(p[2]) == 0:
        p[0] = p[1]
    else:
        # Add the expression to the operands
        p[2][0].insert(0, p[1])
        p[0] = Comparison(p, p[2][0], p[2][1])


def p_comparison_r_multi(p):
    '''comparison_r_multi : comparison_r_multi comparison_r
                          | empty
    '''
    if len(p) == 2:
        p[0] = []
    else:
        if len(p[1]) == 2:
            p[1][0].extend(p[2][0])  # Extend the operands
            p[1][1].extend(p[2][1])  # Extend the operators
        else:
            p[1].append(p[2][0])
            p[1].append(p[2][1])
        p[0] = p[1]


def p_comparison_r(p):
    '''comparison_r : comp_operator a_expr
    '''
    p[0] = [
        [p[2]],  # Operand
        [p[1]]  # Operator
    ]


def p_comp_operator(p):
    '''comp_operator : EQ
                     | NEQ
                     | GT
                     | LT
                     | GTE
                     | LTE
    '''
    p[0] = p[1]


# ---------------- Conditional expressions  -----------------


def p_expression(p):
    '''expression : or_test
                  | or_test IF or_test expression_else
    '''
    if len(p) == 2:
        p[0] = p[1]
    else:
        if p[4] is None:
            p[4] = p[1]
        p[0] = IfElseExpr(p, p[1], p[3], p[4])


def p_expression_if_error(p):
    '''expression : or_test IF error expression_else
    '''

    p.parser.add_error(p, 0, 'Bad expression in if clause of IF ELSE expression')

    if p[4] is None:
            p[4] = p[1]

    p[0] = IfElseExpr(p, p[1], Boolean(p, 0), p[4])


def p_expression_else(p):
    '''expression_else : ELSE expression
    '''
    p[0] = p[2]


def p_expression_else_error(p):
    '''expression_else : ELSE error empty
    '''

    p.parser.add_error(p, 0, 'Bad expression in else clause of IF ELSE expression')

    p[0] = None



def p_expression_opt(p):
    '''expression_opt : expression
                      | empty
    '''
    p[0] = p[1]

# ---------------- Boolean operations -----------------


def p_or_test(p):
    '''or_test : and_test
               | or_test OR and_test
    '''
    if len(p) == 2:
        p[0] = p[1]
    else:
        p[0] = OrTest(p, p[1], p[3])


def p_and_test(p):
    '''and_test : not_test
                | and_test AND not_test
    '''
    if len(p) == 2:
        p[0] = p[1]
    else:
        p[0] = AndTest(p, p[1], p[3])


def p_not_test(p):
    '''not_test : comparison
                | NOT not_test
    '''
    if len(p) == 2:
        p[0] = p[1]
    else:
        p[0] = NotTest(p, p[2])


# -----------------------------------------------------
# ---------------- Simple statements ------------------
# -----------------------------------------------------


def p_simple_stmt(p):
    '''simple_stmt : assignment_stmt
                   | array_stmt
                   | aug_assign_stmt
                   | print_stmt
                   | return_stmt
                   | break_stmt
                   | continue_stmt
                   | call
                   | empty
    '''
    p[0] = p[1]


def p_simple_stmt_error(p):
    '''simple_stmt : assignment_stmt error empty
                   | array_stmt error empty
                   | aug_assign_stmt error empty
                   | print_stmt error empty
                   | return_stmt error empty
                   | call error empty
    '''

    p.parser.add_error(p, 0, "Bad statement")
    p[0] = p[1]


def p_array_stmt(p):  # (target "=")+ "array" "[" integer "]" [array_init_stmt]
    '''array_stmt : target_eq_plus ARRAY LSQUARE INTEGER RSQUARE array_init_stmt_opt
    '''
    try:
        p[0] = AssignmentStatement(p,
                                   p[1],
                                   ArrayInit(p, p[4], p[6]))

    except Exception as e:

        p.parser.add_error(p, 6, e.message, None)

        p[0] = AssignmentStatement(p,
                                   p[1],
                                   ArrayInit(p, p[4], None))


def p_array_init_stmt_opt(p):  # array_init_stmt ?
    '''array_init_stmt_opt : array_init_stmt
                           | empty
    '''
    p[0] = p[1]


def p_array_init_stmt(p):  # "{" expression ("," expression)* "}"
    '''array_init_stmt : LBRAQU expression coma_expr_multi RBRAQU
    '''
    if p[3] is None:
        p[3] = []

    r = [p[2]]
    r.extend(p[3])

    p[0] = r


def p_assignment_stmt(p):  # (target "=")+ expression
    '''assignment_stmt : target_eq_plus expression
    '''
    p[0] = AssignmentStatement(p, p[1], p[2])


def p_assignment_stmt_error(p):  # (target "=")+ error
    '''assignment_stmt : target_eq_plus error empty
    '''
    p.parser.add_error(p, 0, "Bad expression to assign")
    p[0] = AssignmentStatement(p, p[1], Integer(p, 0))


def p_assignment_stmt_opt(p):
    '''assignment_stmt_opt : assignment_stmt
                           | empty
    '''
    p[0] = p[1]


def p_coma_expr_multi(p):  # ("," expression)*
    '''coma_expr_multi : coma_expr_multi COMMA expression
                       | empty
    '''
    if len(p) == 2:
        p[0] = []
    else:
        p[1].append(p[3])
        p[0] = p[1]

# def p_coma_expr_multi_error(p):  # ("," expression)*
#     '''coma_expr_multi : coma_expr_multi error empty expression
#     '''
#     p.parser.add_error(p, 2, 'Unexpected character, a comma was expected')
#     p[0] = p[1]


def p_target_eq_plus(p):  # (target "=")+
    '''target_eq_plus : target_eq_plus target_eq
                      | target_eq
    '''

    if len(p) == 2:
        p[0] = [p[1]]
    else:
        p[1].append(p[2])
        p[0] = p[1]


def p_target_eq(p):  # target "="
    '''target_eq : target EQUAL
    '''
    p[0] = p[1]


def p_target(p):
    '''target : identifier
              | subscription
    '''
    p[0] = p[1]


def p_aug_assign_stmt(p):
    '''aug_assign_stmt : target augop expression
    '''
    operation = BinaryExpr(p, p[1], p[2][:-1], p[3])

    assign_identifier = Identifier(p, p[1].name)

    p[0] = AssignmentStatement(p, [assign_identifier], operation)


def p_aug_assign_stmt_opt(p):
    '''aug_assign_stmt_opt : aug_assign_stmt
                           | empty
    '''
    p[0] = p[1]


def p_augop(p):
    '''augop : AUGOPPLUS
             | AUGOPMINUS
             | AUGOPTIMES
             | AUGOPDIVIDE
             | AUGOPDIVINT
             | AUGOPEXP
             | AUGOPMOD
    '''
    p[0] = p[1]


def p_print_stmt(p):
    '''print_stmt : PRINT print_stmt_opt
    '''
    p[0] = PrintStatement(p, p[2])


def p_print_stmt_opt(p):
    '''print_stmt_opt : empty
                      | expression coma_expr_multi comma_opt
    '''
    if len(p) == 2:
        expressions = []

    else:
        expressions = p[2]
        p[2].insert(0, p[1])

    p[0] = expressions


def p_comma_opt(p):
    '''comma_opt : COMMA
                 | empty
    '''


def p_return_stmt(p):
    '''return_stmt : RETURN expression
                   | RETURN empty
    '''

    if p[2] is None:
        p[2] = Void(p)

    p[0] = ReturnStatement(p, p[2])


def p_break_stmt(p):
    '''break_stmt : BREAK
    '''
    p[0] = BreakStatement(p)


def p_break_stmt_error(p):
    '''break_stmt : BREAK error empty
    '''
    p.parser.add_error(p, 0, "Malformed break statement")
    p[0] = BreakStatement(p)


def p_continue_stmt(p):
    '''continue_stmt : CONTINUE
    '''
    p[0] = ContinueStatement(p)


def p_continue_stmt_error(p):
    '''continue_stmt : CONTINUE error empty
    '''
    p.parser.add_error(p, 0, "Malformed continue statement")
    p[0] = ContinueStatement(p)


# ---------------- Compound statements -----------------


def p_compound_stmt(p):
    '''compound_stmt : if_stmt
                     | while_stmt
                     | for_stmt
                     | forto_stmt
                     | funcdef
    '''
    p[0] = p[1]


def p_block(p):
    '''block : NEWLINE INDENT statement_plus DEDENT
    '''
    p[0] = StatementList(p, p[3])


def p_block_error(p):
    '''block : NEWLINE INDENT error statement_plus DEDENT
    '''
    p.parser.add_error(p, 2, "Unexpected indentation")
    p[0] = StatementList(p, p[4])


def p_block_error_2(p):
    '''block : NEWLINE error empty
    '''
    p.parser.add_error(p, 0, "Expected indentation")
    p[0] = StatementList(p, [])


def p_statement(p):
    '''statement : simple_stmt NEWLINE
                 | compound_stmt
    '''
    p[0] = p[1]


def p_statement_error(p):
    '''statement : simple_stmt INDENT
    '''
    p.parser.add_error(p, 2, "Unexpected indentation")
    p[0] = p[1]


def p_statement_plus(p):
    '''statement_plus : statement_plus statement
                      | statement
    '''
    if len(p) == 2:
        if p[1] is not None:
            p[0] = [p[1]]
        else:
            p[0] = []
    else:

        if p[2] is not None:
            p[1].append(p[2])

        p[0] = p[1]


def p_if_stmt(p):  # if <expr> <block> (else if <expr> <block>)* [else <block>]
    '''if_stmt : IF expression block else_if_stmt_multi else_stmt_opt
    '''

    # replace the 'else' block with the if of the 'else if' block

    if len(p[4]) > 0:  # elseif

        lasti = len(p[4]) - 1

        for i in range(0, lasti):
            p[4][i].c = p[4][i + 1]

        p[4][lasti].c = p[5]

        p[5] = p[4][0]  # 'else' block become the 'else if' block

    p[0] = IfStatement(p, p[2], p[3], p[5])


def p_if_stmt_expr_erro(p):  # if <expr> <block> (else if <expr> <block>)* [else <block>]
    '''if_stmt : IF error block else_if_stmt_multi else_stmt_opt
    '''
    p.parser.add_error(p, 1, 'Bad expression for the'
                + ' \'if\' statement')

    # replace the 'else' block with the if of the 'else if' block

    p[2] = Boolean(p, 1)

    p_if_stmt(p)


def p_else_if_stmt(p):  # else if <expr> <block>
    '''else_if_stmt : ELSE IF expression block
    '''
    p[0] = IfStatement(p, p[3], p[4], None)


def p_else_if_stmt_expr_error(p):
    '''else_if_stmt : ELSE IF error block
    '''

    p.parser.add_error(p, 2, 'Bad expression for the'
                + ' \'else if\' statement')

    p[0] = IfStatement(p, Boolean(p, 1), p[4], None)


def p_else_if_stmt_multi(p):  # (else_if_stmt)*
    '''else_if_stmt_multi : else_if_stmt_multi else_if_stmt
                          | empty
    '''
    if len(p) == 2:
        p[0] = []
    else:
        p[1].append(p[2])
        p[0] = p[1]


def p_else_stmt(p):  # else <block>
    '''else_stmt : ELSE block
    '''
    p[0] = p[2]


def p_else_stmt_opt(p):  # [else_stmt]
    '''else_stmt_opt : else_stmt
                     | empty
    '''
    p[0] = p[1]


def p_while_stmt(p):
    '''while_stmt : WHILE expression block
    '''
    p[0] = ForStatement(p, None, p[2], None, p[3])


def p_while_stmt_error(p):
    '''while_stmt : WHILE error block
    '''

    p.parser.add_error(p, 1, 'Bad expression in while', None)

    p[0] = ForStatement(p, None, None, None, p[3])


def p_for_stmt(p):  # for a ; b ; c do d
    '''for_stmt : FOR assignment_stmt_opt SEMICOLON expression_opt SEMICOLON aug_assign_stmt_opt block
    '''
    p[0] = ForStatement(p, p[2], p[4], p[6], p[7])


def p_for_stmt_error_2(p):  # for a ; b ; c do d
    '''for_stmt : FOR error SEMICOLON expression_opt SEMICOLON aug_assign_stmt_opt block
                | FOR assignment_stmt_opt SEMICOLON error SEMICOLON aug_assign_stmt_opt block
                | FOR assignment_stmt_opt SEMICOLON expression_opt SEMICOLON error block
                | FOR error SEMICOLON error SEMICOLON aug_assign_stmt_opt block
                | FOR assignment_stmt_opt SEMICOLON error SEMICOLON error block
                | FOR error SEMICOLON error SEMICOLON error block
    '''
    p.parser.add_error(p, 0, 'Syntax Error in \'for\' statement definition')

    p[0] = ForStatement(p, None, None, None, p[7])

def _new_ident_from_target(p, target):

    if isinstance(target, Identifier):
        return Identifier(p, target.name)

    elif isinstance(target, ArrayAccess):
        return Identifier(p, target.array_identifier.name)

    assert(False)


def p_forto_stmt(p):  # for a to b do c
    '''forto_stmt : FOR assignment_stmt TO expression block
    '''
    target = p[2].targets[0]
    increment = BinaryExpr(p,
                           _new_ident_from_target(p, target),
                           '+',
                           Integer(p, 1))

    augop = AssignmentStatement(p,
                                [_new_ident_from_target(p, target)],
                                increment)

    # Prepare the comparison for the while loop
    comparison = Comparison(
        p,
        [
            _new_ident_from_target(p, target),
            p[4]
        ],
        ['<']
    )

    p[0] = ForStatement(p, p[2], comparison, augop, p[5])


def p_forto_stmt_error(p):  # for a to b do c
    '''forto_stmt : FOR error TO expression block
                  | FOR assignment_stmt TO error block
                  | FOR error TO error block
    '''
    p.parser.add_error(p, 0, 'Syntax Error in \'for to\' statement definition')
    p[0] = ForStatement(p, None, None, None, p[5])


def p_funcdef(p):  # function "[" <paramlist> "]" <block>
    '''funcdef : FUNCTION LSQUARE defparam_list RSQUARE block
               | FUNCTION LSQUARE ID RSQUARE block
    '''

    name = ''
    args = []

    if isinstance(p[3], str):
        name = p[3]

        if name == 'main':
            name = '$main'

    else:
        for param in p[3]:
            name += param.description + ':'
            args.append(param.name)

    p[0] = FunctionDefinition(p, name, args, p[5])


def p_defparam_list(p):
    '''defparam_list : defparam_plus
    '''
    p[0] = p[1]


def p_defparam(p):
    '''defparam : paramdesc COLON paramname
    '''
    p[0] = DefParam(p, p[1], p[3])


def p_defparam_plus(p):
    '''defparam_plus : defparam_plus defparam
                     | defparam
    '''
    if len(p) == 2:
        p[0] = [p[1]]
    else:
        p[1].append(p[2])
        p[0] = p[1]


def p_callparam_list(p):
    '''callparam_list : callparam_plus
    '''
    p[0] = p[1]


def p_callparam(p):
    '''callparam : paramdesc COLON paramvalue
    '''
    p[0] = CallParam(p, p[1], p[3])


def p_callparam_plus(p):
    '''callparam_plus : callparam_plus callparam
                      | callparam
    '''
    if len(p) == 2:
        p[0] = [p[1]]
    else:
        p[1].append(p[2])
        p[0] = p[1]


def p_paramdesc(p):
    '''paramdesc : ID
    '''
    p[0] = p[1]


def p_paramname(p):
    '''paramname : identifier
    '''
    p[0] = p[1]


def p_paramvalue(p):
    '''paramvalue : expression
    '''
    p[0] = p[1]


# -----------------------------------------------------


def p_empty(p):
    'empty :'
    pass


def p_error(p):
    pass
