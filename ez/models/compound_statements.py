from ez.models.base import ASTObj
from ez.models.literals import Boolean
from ez.models.types import BooleanType


class StatementList(ASTObj):
    ''' A list of statements '''

    def __init__(self, token, statements=[]):
        super(StatementList, self).__init__(token)

        self.statements = statements
        self.has_return = False
        self.return_statements = []

    def __repr__(self):
        return '\n'.join([repr(stmt) for stmt in self.statements])

    def accept_children(self, visitor):
        ''' Tells a visitor to visit the children. '''
        for statement in self.statements:
            statement.accept(visitor)

    def append(self, stmt):
        ''' Appends a statement to the list. '''
        self.statements.append(stmt)

    def _compute_type(self, observable):
        '''
        Compute the type of the current node according to the object type of
        the observable.
        '''
        self._set_obj_type(observable.obj_type)

    def _check_dependencies_types(self):
        '''
        Returns True if the types of all dependencies have all been checked.
        Otherwise,returns False.
        '''
        for dep in self._dependencies:
            if dep.obj_type is None and \
                    not isinstance(dep, ReturnStatement):
                return False

        return True

    def type_did_updated(self, type_obj):
        ''' Tells the current node that its type is known. '''
        self._set_obj_type(type_obj)


class IfStatement(ASTObj):
    '''
    An if statement.

    if a then b else c
    '''

    def __init__(self, token, a, b, c):
        super(IfStatement, self).__init__(token)

        self.a = a
        self.b = b
        self.c = c

    def __repr__(self):
        return 'if {} then {} else {}'.format(self.a, self.b, self.c)

    def accept_children(self, visitor):
        ''' Tells a visitor to visit the children. '''
        self.a.accept(visitor)
        self.b.accept(visitor)

        if self.c is not None:
            self.c.accept(visitor)

    def _compute_type(self, observable):
        '''
        Compute the type of the current node according to the object type of
        the observable.
        '''
        self.check_type(self.a, BooleanType)

        self._set_obj_type(self.b.obj_type)

        if self.c is not None:
            self._set_obj_type(self.c.obj_type)


class ForStatement(ASTObj):
    '''
    A for statement.

    for init ; cond ; increment
        body
    '''

    def __init__(self, token, init, cond, increment, body):
        super(ForStatement, self).__init__(token)

        self.init = init

        if cond is None:
            cond = Boolean(token, 1)

        self.cond = cond
        self.increment = increment
        self.body = body

    def __repr__(self):
        return 'for {} ; {} ; {}\n\t{}'.format(self.init,
                                               self.cond,
                                               self.increment,
                                               self.body)

    def accept_children(self, visitor):
        ''' Tells a visitor to visit the children. '''
        if self.init is not None:
            self.init.accept(visitor)

        self.cond.accept(visitor)

        if self.increment is not None:
            self.increment.accept(visitor)

        self.body.accept(visitor)

    def _compute_type(self, observable):
        '''
        Compute the type of the current node according to the object type of
        the observable.
        '''
        self.check_type(self.cond, BooleanType)
        self._set_obj_type(self.body.obj_type)
