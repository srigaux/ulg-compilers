#!/usr/bin/env python

import argparse

from ez.ezcompiler import EZCompiler


def parse_args():

    # Parse the arguments
    description = 'Compile the given EZ code.'
    arg_parser = argparse.ArgumentParser(description=description)
    arg_parser.add_argument(
        '-d', '--debug',
        dest='debug',
        default=False,
        action='store_true',
        help='enables the debug mode'
    )

    arg_parser.add_argument(
        '-c', '--color',
        dest='color',
        default=False,
        action='store_true',
        help='enables the color mode'
    )

    arg_parser.add_argument(
        '-l', '--errors-context-line',
        dest='errors_context_line',
        default=3,
        help='the number of lines to desplay before and after an error'
    )

    arg_parser.add_argument(
        '-s', '--silent',
        dest='silent',
        default=False,
        action='store_true',
        help='enables the silent mode'
    )

    arg_parser.add_argument(
        'filepath',
        metavar='filepath',
        type=str,
        help='a path to the file to compile'
    )

    arg_parser.add_argument(
        '-o', '--output',
        default='result.ll',
        metavar='<path>',
        dest='output_filepath',
        help='a path to the compiled output file'
    )

    arg_parser.add_argument(
        '-no', '--no-optimization',
        dest='optimize',
        default=True,
        action='store_false',
        help='enables the llvm optimization'
    )

    args = arg_parser.parse_args()

    return args

if __name__ == '__main__':

    args = parse_args()


    compiler = EZCompiler(filepath=args.filepath,
                          color=args.color,
                          optimize=args.optimize,
                          errors_context_line=args.errors_context_line)

    compiler.compile(args.output_filepath)

    exit(0)
