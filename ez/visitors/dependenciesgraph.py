from ez.models.base import *
from ez.models.arrays import *
from ez.models.compound_statements import *
from ez.models.expressions import *
from ez.models.functions import *
from ez.models.literals import *
from ez.models.simple_statements import *
from ez.models.types import *

from ez.util import visit as v
from ez.visitors.base import *


class DependenciesGraphVisitor(ParentFirstVisitor):
    '''
    A visitor that enables to generate some javascript code to display a graph
    of all dependencies using the vis.js library.
    '''

    def __init__(self, file_path):
        super(DependenciesGraphVisitor, self).__init__()

        self.nodes = {}
        self.edges = []
        self.file_path = file_path

    def js_code(self):
        code = ''
        code += 'nodes = ' +  str(self.nodes.values()) + ';'
        code += 'edges = ' +  str(self.edges)          + ';'
        return code

    def save_js_code(self):
        f = open(self.file_path, 'w+')
        f.write(self.js_code())
        f.close()

    def visit(self, node):

        if self.nodes.get(id(node)) is not None:
            return

        node_dict = {
            'id': id(node),
            'label': node._type + '\n' + str(node)[:20],
            'title': str(node).replace('\n', '<br>'),
            'shape': 'box',
        }

        self.nodes[id(node)] = self.update_node_dict(node, node_dict)[0]

        for dep in node._observers:
            self.edges.append({
                              'from': id(node),
                              'to': id(dep)
                              })

    @v.on('node')
    def update_node_dict(self, node, node_dict):
        pass

    @v.when(ASTObj)
    def update_node_dict(self, node, node_dict):
        return node_dict

    @v.when(Program)
    def update_node_dict(self, node, node_dict):
        return [node_dict]

    @v.when(UniqueIdentifier)
    def update_node_dict(self, node, node_dict):
        node_dict['color'] = 'darkorange'
        return [node_dict]

    @v.when(Identifier)
    def update_node_dict(self, node, node_dict):
        node_dict['color'] = 'f39c12'
        return [node_dict]

    @v.on('parent')
    def will_accept_children(self, parent):
        pass

    @v.when(Program)
    def will_accept_children(self, parent):
        for cls, t in Type.instances.items():
            self.nodes[id(t)] = {
                'id': id(t),
                'label': t.__class__.__name__,
                'color': 'red'
            }

            dependencies = []

            dependencies.extend(t.observers)

            if isinstance(t, FunctionType):
                dependencies.append(t._definition)
                dependencies.extend(t._definition_observers)

            if isinstance(t, ContainerType):
                if t.inner_type is not None:
                    dependencies.append(t.inner_type)
                dependencies.extend(t.inner_type_observers)

            for dep in dependencies:
                self.edges.append({
                                  'from': id(t),
                                  'to': id(dep),
                                  'color': 'red'
                                  })

    @v.on('parent')
    def did_accept_children(self, parent):
        pass

    @v.when(Program)
    def did_accept_children(self, parent):
        self.save_js_code()
