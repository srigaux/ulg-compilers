# EZ Compiler
***
- Group : 1
- Purpose : Lexical and syntactic analysis
- Deadline : **April 4, 2014 (23h59)**

## Description

In this project, you will realize a compiler for a programming language of your own design. This language must be expressive enough, so that you can write easily an algorithm such as QuickSort with it.
***
## Install
Install pip and virtualenv for Ubuntu 10.10 Maverick and newer :
```bash
$ sudo apt-get install python-pip python-dev build-essential 
$ sudo pip install --upgrade pip 
$ sudo pip install --upgrade virtualenv
```
For older versions of Ubuntu :
```bash
$ sudo apt-get install python-setuptools python-dev build-essential
$ sudo easy_install pip 
$ sudo pip install --upgrade virtualenv 
```
For MacOS :
```bash
$ sudo easy_install build-essential
```

Install PLY module :
```bash
$ sudo pip install ply
```
***

## Useful links
+ ### General
    - [Assignment page](http://www.montefiore.ulg.ac.be/~info0085/)
    - [Theoretical course page](http://www.montefiore.ulg.ac.be/~geurts/Cours/compil/2013/compil2013_2014.html)
    - [Google Drive General Document](https://docs.google.com/document/d/1hZWgD6Nm5qjUBTr-TB3oJQI6A4m-nUyg4_s3qKfwkpQ/edit)
    - [Google Drive Grammar Report](https://docs.google.com/document/d/1LdI7vuY2rIBThJDPF6kgIZKSkRG5uResam7uRUBAzJU/edit)

+ ### Python
    - [Python Doc](http://docs.python.org/2/)
    - [Python Grammar](http://inst.eecs.berkeley.edu/~cs164/fa11/python-grammar.html)
    - [PLY (Python Lex Yacc)](http://www.dabeaz.com/ply/)
    - [Python Source Code (Github)](https://github.com/python-git/python)
    - [PLY Tuto (French)](http://www.matthieuamiguet.ch/pages/compilateurs)
