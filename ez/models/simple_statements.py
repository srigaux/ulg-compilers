from ez.models.base import ASTObj
from ez.semantic.error import EZSemanticError
from ez.models.types import VoidType


class AssignmentStatement(ASTObj):
    '''
    An assignment statement.

    a[1] = a[2] ... = b
    '''

    def __init__(self, token, targets, value):
        super(AssignmentStatement, self).__init__(token)

        self.targets = targets
        self.value = value

        for target in self.targets:
            target.is_assignment = True

    def __repr__(self):
        return '{} = {}'.format(self.targets, self.value)

    def accept_children(self, visitor):
        ''' Tells a visitor to visit the children. '''
        for target in self.targets:
            target.accept(visitor)

        self.value.accept(visitor)

    def _compute_type(self, observable):
        '''
        Compute the type of the current node according to the object type of
        the observable.
        '''
        value_type = self.value.obj_type

        if isinstance(value_type, VoidType):
            raise EZSemanticError(self, BadAssignmentValueError(self))

        self._set_obj_type(value_type)

    def _set_obj_type(self, obj_type):
        '''
        Initializes the object type of the current node and sets the inner type
        of the observers ArrayAccess.
        '''
        from ez.models.arrays import ArrayAccess

        for observer in self._observers:
            if isinstance(observer, ArrayAccess):
                    observer.inner_type = obj_type

        super(AssignmentStatement, self)._set_obj_type(obj_type)


class BadAssignmentValueError(Exception):
    ''' An error that is raised when we tried to assign a bad value. '''

    def __init__(self, assignment):

        message = 'Try to assign value of type : {}' \
            .format(assignment.value.obj_type)

        super(BadAssignmentValueError, self).__init__(message)

        self.assignment = assignment


class PrintStatement(ASTObj):
    '''
    A print statement.

    print a[0], a[1], ...
    '''

    def __init__(self, token, expressions=[]):
        super(PrintStatement, self).__init__(token)

        self.expressions = expressions

    def __repr__(self):
        return 'print {}'.format(self.expressions)

    def accept_children(self, visitor):
        ''' Tells a visitor to visit the children. '''

        for expr in self.expressions:
            expr.accept(visitor)

    def type_did_updated(self, obj_type):
        ''' Tells the current node that its type is known. '''
        self._set_obj_type(obj_type)


class BreakStatement(ASTObj):
    ''' A break statement. '''

    def __init__(self, token):
        super(BreakStatement, self).__init__(token)

    def __repr__(self):
        return 'break'


class ContinueStatement(ASTObj):
    ''' A continue statement. '''

    def __init__(self, token):
        super(ContinueStatement, self).__init__(token)

    def __repr__(self):
        return 'continue'
