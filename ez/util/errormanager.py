# -*- coding: utf-8 -*-

from ez.errors.ezerror import EZError


class ErrorManager(object):
    ''' Manages all the errors that may occur during the compilation. '''

    def __init__(self, content=None, color=False, errors_context_line=3):
        super(ErrorManager, self).__init__()
        self.errors = []
        self.color = color
        self.set_content(content)
        self.errors_context_line = errors_context_line

    @staticmethod
    def manager(content=None, color=False,
                errors_context_line=3):

        return ErrorManager(content=content, color=color)

    def has_error(self):
        ''' Returns True if the manager contains at least an error. '''
        for error in self.errors:
            if error.level > EZError.WARNING:
                return True

    def add_error(self, error, raise_error=False):
        ''' Adds an error to the manager. '''
        self.errors.append(error)

        if raise_error:
            raise error

    def set_content(self, content):
        ''' Sets the source file content. '''
        self.content = content

        if content is None:
            self.content_length = 0
            self.content_lines = []
            self.content_lines_start = []

        else:
            self.content_lines = content.splitlines(True)

            self.content_length = 0
            self.content_lines_start = []
            for line in self.content_lines:
                self.content_lines_start.append(self.content_length)
                self.content_length += len(line)

    def _get_error_range(self, error):
        '''
        Returns the start line, start column, end line and end column from a
        tuple.

        This is used to delimit the portion of code to display.
        '''
        source = error.source

        if isinstance(source, tuple):

            assert(len(source) == 2)

            if (isinstance(source[0], tuple)):
                (sline, scol), (eline, ecol) = source
            else:

                cstart, cend = source

                sline, scol = self._find_column(cstart)
                eline, ecol = self._find_column(cend)

                sline += 1
                scol += 1
                eline += 1
                ecol += 1

            return ((sline, scol), (eline, ecol))

    def _find_column(self, pos):
        '''
        Computes the column number of position.

        Args:
            pos (int): the position

        Returns:
            the column number of the token
        '''

        for i, line_start in enumerate(self.content_lines_start):
            if line_start > pos:
                return (i - 1, pos - self.content_lines_start[i-1])

        if line_start + len(self.content_lines[i]) >= pos:
            return (i - 1, pos - self.content_lines_start[i-1])

    def print_errors(self):
        '''
        Displays a list of errors/warnings and adds the location of each
        error/warning.
        '''

        context = self.errors_context_line

        for error in self.errors:

            ((sl, sc), (el, ec)) = self._get_error_range(error)

            s = (len(error.level.name) + 3) * ' '
            msg = s + 'at line {} and column {}'.format(sl, sc)

            if error.details is not None:
                msg += '\n\n' + s + error.details.replace('\n', '\n' + s)

            if self.color:
                default_color = '\033[0m'
                title_color = error.level.title_color
                source_color = error.level.source_color
            else:
                default_color = ''
                title_color = ''
                source_color = ''

            if context * 2 + el-sl + 1 > len(self.content_lines) - 1:
                lines = list(self.content_lines)[:-1]
                scl = 0
            else:
                scl, ecl = sl - context - 1, el + context

                offset = 0

                if scl < 0:
                    offset = -scl
                elif ecl > len(self.content_lines) - 1:
                    offset = len(self.content_lines) - ecl - 1

                scl, ecl = scl + offset, ecl + offset

                lines = self.content_lines[scl:ecl]

            lines[el-scl-1] = \
                lines[el-scl-1][:ec-1] + \
                default_color + \
                lines[el-scl-1][ec-1:]

            lines[sl-scl-1] = \
                lines[sl-scl-1][:sc-1] + \
                source_color + \
                lines[sl-scl-1][sc-1:]

            print
            print '{}{} : {}\n{}{}\n'.format(title_color,
                                             error.level.name,
                                             error.message,
                                             msg,
                                             default_color)

            for i, line in enumerate(lines):

                color = source_color if i > sl-scl-1 and i <= el-scl-1 else ''

                print '\t{}{:>3}|{}{}{}{}'.format(title_color,
                                                  i + scl + 1,
                                                  default_color,
                                                  color,
                                                  line[:-1],
                                                  default_color)

                if i == sl-scl-1 and not self.color:
                    print '\t {}^'.format(' ' * (sc-1 + 3))
