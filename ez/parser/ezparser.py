import sys
import pprint

from ply.lex import LexToken
from ply.yacc import *

from ez.parser import rules
from ez.errors.ezerror import EZError
from ez.util.console import HistoryConsole


class EZParserError(EZError):
    ''' An error related to the parsing part of the compilation. '''
    def __init__(self, source, inner, level=EZError.ERROR, details=None):
        super(EZParserError, self).__init__(source, inner, level, details)


class EZParser(LRParser):
    ''' A parser for the EZ language. '''

    def __init__(self,
                 lrtable,
                 error_func,
                 lexer,
                 debug,
                 silent,
                 error_manager):

        LRParser.__init__(self, lrtable, error_func)

        self.debug = debug
        self.silent = silent
        self.error_manager = error_manager

        # Build the lexer
        self.lexer = lexer

    def add_error(self,
                  tokens,
                  index,
                  message=None,
                  details=None,
                  is_warning=False):
        ''' Adds an error to the error manager. '''

        if self.error_manager is None:
            return

        if message is None:
            message = 'Unexpected token \'{}\''.format(tokens[index].value)

        level = EZError.WARNING if is_warning else EZError.ERROR

        if hasattr(tokens.slice[index], 'endlexpos'):
            source = (tokens.slice[index].lexpos,
                      tokens.slice[index].endlexpos)

        else:
            source = (tokens.slice[index].lexpos,
                      tokens.slice[index].lexpos + 1)

        error = EZParserError(source, message, level, details)

        self.error_manager.add_error(error)

    def parse(self, content):
        ''' Start the parsing of some content. ''' 
        result = LRParser.parse(self,
                                content,
                                debug=self.debug,
                                lexer=self.lexer,
                                tracking=True)
        return result

    def parse_loop(self):
        ''' Launches an interactive console to parse input on the fly. '''
        # Initialize a console and load the history file
        console = HistoryConsole('parser.hist')

        # Main loop
        while True:
            try:
                string = console.raw_input('>>> ')

                if string != '':
                    string += '\n'

                    while True:
                        tmp = console.raw_input('... ')

                        if tmp == '':
                            break
                        else:
                            string += tmp + '\n'

            except EOFError:
                break
            if not string:
                continue
            result = self.parse(string)
            print pprint.pprint(result)

    @staticmethod
    def parser(lexer=False,
               debug=False,
               silent=False,
               error_manager=None):
        ''' Builds a parser (See yacc module of PLY). '''

        method = 'LALR'
        module = rules
        tabmodule = 'parsetab'
        start = None
        check_recursion = 1
        optimize = 0
        write_tables = 1
        debugfile = 'parser.out'
        outputdir = ''
        debuglog = None
        errorlog = None
        picklefile = None

        global parse                 # Reference to the parsing method of the last built parser

        # If pickling is enabled, table files are not created

        if picklefile:
            write_tables = 0

        if errorlog is None:
            errorlog = PlyLogger(sys.stderr)

        # Get the module dictionary used for the parser
        if module:
            _items = [(k, getattr(module, k)) for k in dir(module)]
            pdict = dict(_items)
        else:
            pdict = get_caller_module_dict(2)

        # Collect parser information from the dictionary
        pinfo = ParserReflect(pdict, log=errorlog)
        pinfo.get_all()

        if pinfo.error:
            raise YaccError("Unable to build parser")

        # Check signature against table files (if any)
        signature = pinfo.signature()

        # Read the tables
        try:
            lr = LRTable()
            if picklefile:
                read_signature = lr.read_pickle(picklefile)
            else:
                read_signature = lr.read_table(tabmodule)
            if optimize or (read_signature == signature):
                try:
                    lr.bind_callables(pinfo.pdict)
                    parser = EZParser(lr,
                                      pinfo.error_func,
                                      lexer,
                                      debug,
                                      silent,
                                      error_manager)
                    parse = parser.parse
                    return parser
                except Exception:
                    e = sys.exc_info()[1]
                    errorlog.warning("There was a problem " +
                                     "loading the table file: %s", repr(e))
        except VersionError:
            e = sys.exc_info()
            errorlog.warning(str(e))
        except Exception:
            pass

        if debuglog is None:
            if debug:
                debuglog = PlyLogger(open(debugfile, "w"))
            else:
                debuglog = NullLogger()

        errors = 0

        # Validate the parser information
        if pinfo.validate_all():
            raise YaccError("Unable to build parser")

        if not pinfo.error_func:
            errorlog.warning("no p_error() function is defined")

        # Create a grammar object
        grammar = Grammar(pinfo.tokens)

        # Set precedence level for terminals
        for term, assoc, level in pinfo.preclist:
            try:
                grammar.set_precedence(term, assoc, level)
            except GrammarError:
                e = sys.exc_info()[1]
                errorlog.warning("%s", str(e))

        # Add productions to the grammar
        for funcname, gram in pinfo.grammar:
            file, line, prodname, syms = gram
            try:
                grammar.add_production(prodname, syms, funcname, file, line)
            except GrammarError:
                e = sys.exc_info()[1]
                errorlog.error("%s", str(e))
                errors = 1

        # Set the grammar start symbols
        try:
            if start is None:
                grammar.set_start(pinfo.start)
            else:
                grammar.set_start(start)
        except GrammarError:
            e = sys.exc_info()[1]
            errorlog.error(str(e))
            errors = 1

        if errors:
            raise YaccError("Unable to build parser")

        # Verify the grammar structure
        undefined_symbols = grammar.undefined_symbols()
        for sym, prod in undefined_symbols:
            errorlog.error("%s:%d: Symbol '%s' used, but not defined " +
                           "as a token or a rule", prod.file, prod.line, sym)
            errors = 1

        unused_terminals = grammar.unused_terminals()
        if unused_terminals:
            debuglog.info("")
            debuglog.info("Unused terminals:")
            debuglog.info("")
            for term in unused_terminals:
                errorlog.warning("Token '%s' defined, but not used", term)
                debuglog.info("    %s", term)

        # Print out all productions to the debug log
        if debug:
            debuglog.info("")
            debuglog.info("Grammar")
            debuglog.info("")
            for n, p in enumerate(grammar.Productions):
                debuglog.info("Rule %-5d %s", n, p)

        # Find unused non-terminals
        unused_rules = grammar.unused_rules()
        for prod in unused_rules:
            errorlog.warning("%s:%d: Rule '%s' defined, but not used",
                             prod.file,
                             prod.line,
                             prod.name)

        if len(unused_terminals) == 1:
            errorlog.warning("There is 1 unused token")
        if len(unused_terminals) > 1:
            errorlog.warning("There are %d unused tokens",
                             len(unused_terminals))

        if len(unused_rules) == 1:
            errorlog.warning("There is 1 unused rule")
        if len(unused_rules) > 1:
            errorlog.warning("There are %d unused rules", len(unused_rules))

        if debug:
            debuglog.info("")
            debuglog.info("Terminals, with rules where they appear")
            debuglog.info("")
            terms = list(grammar.Terminals)
            terms.sort()
            for term in terms:
                debuglog.info("%-20s : %s",
                              term,
                              " ".join([str(s)
                                       for s
                                       in grammar.Terminals[term]]
                                       ))

            debuglog.info("")
            debuglog.info("Nonterminals, with rules where they appear")
            debuglog.info("")
            nonterms = list(grammar.Nonterminals)
            nonterms.sort()
            for nonterm in nonterms:
                debuglog.info("%-20s : %s",
                              nonterm,
                              " ".join([str(s)
                                       for s
                                       in grammar.Nonterminals[nonterm]]
                                       ))
            debuglog.info("")

        if check_recursion:
            unreachable = grammar.find_unreachable()
            for u in unreachable:
                errorlog.warning("Symbol '%s' is unreachable", u)

            infinite = grammar.infinite_cycles()
            for inf in infinite:
                errorlog.error("Infinite recursion " +
                               "detected for symbol '%s'", inf)
                errors = 1

        unused_prec = grammar.unused_precedence()
        for term, assoc in unused_prec:
            errorlog.error("Precedence rule '%s' defined " +
                           "for unknown symbol '%s'", assoc, term)
            errors = 1

        if errors:
            raise YaccError("Unable to build parser")

        # Run the LRGeneratedTable on the grammar
        if debug:
            errorlog.debug("Generating %s tables", method)

        lr = LRGeneratedTable(grammar, method, debuglog)

        if debug:
            num_sr = len(lr.sr_conflicts)

            # Report shift/reduce and reduce/reduce conflicts
            if num_sr == 1:
                errorlog.warning("1 shift/reduce conflict")
            elif num_sr > 1:
                errorlog.warning("%d shift/reduce conflicts", num_sr)

            num_rr = len(lr.rr_conflicts)
            if num_rr == 1:
                errorlog.warning("1 reduce/reduce conflict")
            elif num_rr > 1:
                errorlog.warning("%d reduce/reduce conflicts", num_rr)

        # Write out conflicts to the output file
        if debug and (lr.sr_conflicts or lr.rr_conflicts):
            debuglog.warning("")
            debuglog.warning("Conflicts:")
            debuglog.warning("")

            for state, tok, resolution in lr.sr_conflicts:
                debuglog.warning("shift/reduce conflict for " +
                                 "%s in state %d resolved as %s",
                                 tok,
                                 state,
                                 resolution)

            already_reported = {}
            for state, rule, rejected in lr.rr_conflicts:
                if (state, id(rule), id(rejected)) in already_reported:
                    continue
                debuglog.warning("reduce/reduce conflict in state %d " +
                                 "resolved using rule (%s)", state, rule)

                debuglog.warning("rejected rule (%s) in state %d",
                                 rejected, state)

                errorlog.warning("reduce/reduce conflict in state %d " +
                                 "resolved using rule (%s)", state, rule)

                errorlog.warning("rejected rule (%s) in state %d",
                                 rejected, state)

                already_reported[state, id(rule), id(rejected)] = 1

            warned_never = []
            for state, rule, rejected in lr.rr_conflicts:
                if not rejected.reduced and (rejected not in warned_never):
                    debuglog.warning("Rule (%s) is never reduced", rejected)
                    errorlog.warning("Rule (%s) is never reduced", rejected)
                    warned_never.append(rejected)

        # Write the table file if requested
        if write_tables:
            lr.write_table(tabmodule, outputdir, signature)

        # Write a pickled version of the tables
        if picklefile:
            lr.pickle_table(picklefile, signature)

        # Build the parser
        lr.bind_callables(pinfo.pdict)
        parser = EZParser(lr,
                          pinfo.error_func,
                          lexer,
                          debug,
                          silent,
                          error_manager)

        parse = parser.parse
        return parser
