import sys

from ez.models.base import *
from ez.models.arrays import *
from ez.models.compound_statements import *
from ez.models.expressions import *
from ez.models.functions import *
from ez.models.literals import *
from ez.models.simple_statements import *

from ez.util import visit as v
from ez.visitors.base import ParentFirstVisitor


class PrettyPrintVisitor(ParentFirstVisitor):

    def __init__(self):
        super(PrettyPrintVisitor, self).__init__()

        self.indent_count = 0

    def _print_indents(self):
        sys.stdout.write('\t' * self.indent_count)

    def will_accept_children(self, parent):
        self.indent_count += 1

    def did_accept_children(self, parent):
        self.indent_count -= 1

    def visit(self, node):
        self._print_indents()
        sys.stdout.write('[' + node._type + "] ")
        self._visit(node)

    @v.on('node')
    def _visit(self, node):
        '''This is the generic method that initializes the dynamic dispatcher.
        '''

    # -------------------------------------------------------------------------
    #                              BASE
    # -------------------------------------------------------------------------
    @v.when(ASTObj)
    def _visit(self, node):
        ''' ASTObj '''
        print

    @v.when(UniqueIdentifier)
    def _visit(self, uid):
        ''' Identifier '''
        print uid.repr_dict()

    @v.when(Identifier)
    def _visit(self, ident):
        ''' Identifier '''
        print ident.name, 'assign' if ident.is_assignment else ''

    # -------------------------------------------------------------------------
    #                              LITERALS
    # -------------------------------------------------------------------------

    @v.when(Integer)
    def _visit(self, i):
        ''' Integer '''
        print i.value

    @v.when(Float)
    def _visit(self, f):
        ''' Float '''
        print f.value

    @v.when(Boolean)
    def _visit(self, boolean):
        ''' Boolean '''
        print boolean.value

    # -------------------------------------------------------------------------
    #                               ARRAYS
    # -------------------------------------------------------------------------
    @v.when(ArrayInit)
    def _visit(self, array):
        ''' ArrayInit '''
        print '[{}]'.format(array.length), array.values

    @v.when(ArrayAccess)
    def _visit(self, array):
        ''' ArrayAccess '''
        print '{}[{}]{}'.format(array.array_identifier,
                                array.index,
                                'assign' if array.is_assignment else '')

    # -------------------------------------------------------------------------
    #                              EXPRESSIONS
    # -------------------------------------------------------------------------

    @v.when(UnaryExpr)
    def _visit(self, expr):
        ''' UnaryExpr '''
        print expr.operator

    @v.when(BinaryExpr)
    def _visit(self, expr):
        ''' BinaryExpr '''
        print expr.operator

    @v.when(Comparison)
    def _visit(self, expr):
        ''' Comparison '''
        print expr.operators

    # -------------------------------------------------------------------------
    #                          SIMPLE STATEMENTS
    # -------------------------------------------------------------------------

    # -------------------------------------------------------------------------
    #                           COMPOUND STATEMENTS
    # -------------------------------------------------------------------------

    # -------------------------------------------------------------------------
    #                              FUNCTIONS
    # -------------------------------------------------------------------------

    @v.when(FunctionCall)
    def _visit(self, function):
        ''' FunctionCall '''
        print '{}'.format(function.name)

    @v.when(FunctionDefinition)
    def _visit(self, function):
        ''' FunctionDefinition '''
        print '{}{}'.format(function.name, function.args)
