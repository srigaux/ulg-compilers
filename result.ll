; ModuleID = 'main'

@YesFormat = global [4 x i8] c"Yes\00"
@NoFormat = global [3 x i8] c"No\00"
@a = global i32* null
@format = global [34 x i8] c"[%d %d %d %d %d %d %d %d %d %d] \0A\00"

declare i32 @printf(i8*, ...)

define void @"swapObjectAtIndex:withObjectAtIndex:inArray:"(i32, i32, i32*) {
entry:
  %3 = sext i32 %1 to i64
  %4 = getelementptr i32* %2, i64 %3
  %array_access = load i32* %4, align 4
  %5 = sext i32 %0 to i64
  %6 = getelementptr i32* %2, i64 %5
  %array_access4 = load i32* %6, align 4
  store i32 %array_access4, i32* %4, align 4
  store i32 %array_access, i32* %6, align 4
  ret void
}

define void @"quicksortArray:size:"(i32*, i32) {
entry:
  %subtmp = add i32 %1, -1
  call void @"quicksortArray:fromIndex:toIndex:"(i32* %0, i32 0, i32 %subtmp)
  ret void
}

define i32 @"partitionArray:fromIndex:toIndex:"(i32*, i32, i32) {
entry:
  %3 = sext i32 %2 to i64
  %4 = getelementptr i32* %0, i64 %3
  %array_access = load i32* %4, align 4
  %subtmp = add i32 %1, -1
  br label %loop

loop:                                             ; preds = %after_if, %entry
  %"partitionArray:fromIndex:toIndex:_#_i.0" = phi i32 [ %subtmp, %entry ], [ %"partitionArray:fromIndex:toIndex:_#_i.1", %after_if ]
  %"partitionArray:fromIndex:toIndex:_#_j.0" = phi i32 [ %1, %entry ], [ %addtmp17, %after_if ]
  %5 = sext i32 %"partitionArray:fromIndex:toIndex:_#_j.0" to i64
  %6 = getelementptr i32* %0, i64 %5
  %array_access6 = load i32* %6, align 4
  %int_cmp = icmp sgt i32 %array_access6, %array_access
  br i1 %int_cmp, label %after_if, label %then

afterloop:                                        ; preds = %after_if
  %addtmp19 = add i32 %"partitionArray:fromIndex:toIndex:_#_i.1", 1
  call void @"swapObjectAtIndex:withObjectAtIndex:inArray:"(i32 %addtmp19, i32 %2, i32* %0)
  ret i32 %addtmp19

then:                                             ; preds = %loop
  %addtmp = add i32 %"partitionArray:fromIndex:toIndex:_#_i.0", 1
  call void @"swapObjectAtIndex:withObjectAtIndex:inArray:"(i32 %addtmp, i32 %"partitionArray:fromIndex:toIndex:_#_j.0", i32* %0)
  br label %after_if

after_if:                                         ; preds = %loop, %then
  %"partitionArray:fromIndex:toIndex:_#_i.1" = phi i32 [ %addtmp, %then ], [ %"partitionArray:fromIndex:toIndex:_#_i.0", %loop ]
  %subtmp14 = add i32 %2, -1
  %int_cmp15 = icmp slt i32 %"partitionArray:fromIndex:toIndex:_#_j.0", %subtmp14
  %addtmp17 = add i32 %"partitionArray:fromIndex:toIndex:_#_j.0", 1
  br i1 %int_cmp15, label %loop, label %afterloop
}

define void @"quicksortArray:fromIndex:toIndex:"(i32*, i32, i32) {
entry:
  %int_cmp = icmp slt i32 %1, %2
  br i1 %int_cmp, label %then, label %after_if

then:                                             ; preds = %entry
  %calltmp = call i32 @"partitionArray:fromIndex:toIndex:"(i32* %0, i32 %1, i32 %2)
  %subtmp = add i32 %calltmp, -1
  call void @"quicksortArray:fromIndex:toIndex:"(i32* %0, i32 %1, i32 %subtmp)
  %addtmp = add i32 %calltmp, 1
  call void @"quicksortArray:fromIndex:toIndex:"(i32* %0, i32 %addtmp, i32 %2)
  br label %after_if

after_if:                                         ; preds = %entry, %then
  ret void
}

define i32 @main() {
entry:
  %array = alloca [10 x i32], align 4
  %array_ptr = getelementptr [10 x i32]* %array, i64 0, i64 0
  store i32 9, i32* %array_ptr, align 4
  %it_ptr = getelementptr [10 x i32]* %array, i64 0, i64 1
  store i32 8, i32* %it_ptr, align 4
  %it_ptr1 = getelementptr [10 x i32]* %array, i64 0, i64 2
  store i32 7, i32* %it_ptr1, align 4
  %it_ptr2 = getelementptr [10 x i32]* %array, i64 0, i64 3
  store i32 6, i32* %it_ptr2, align 4
  %it_ptr3 = getelementptr [10 x i32]* %array, i64 0, i64 4
  store i32 5, i32* %it_ptr3, align 4
  %it_ptr4 = getelementptr [10 x i32]* %array, i64 0, i64 5
  store i32 4, i32* %it_ptr4, align 4
  %it_ptr5 = getelementptr [10 x i32]* %array, i64 0, i64 6
  store i32 3, i32* %it_ptr5, align 4
  %it_ptr6 = getelementptr [10 x i32]* %array, i64 0, i64 7
  store i32 2, i32* %it_ptr6, align 4
  %it_ptr7 = getelementptr [10 x i32]* %array, i64 0, i64 8
  store i32 1, i32* %it_ptr7, align 4
  %it_ptr8 = getelementptr [10 x i32]* %array, i64 0, i64 9
  store i32 0, i32* %it_ptr8, align 4
  store i32* %array_ptr, i32** @a, align 8
  call void @"quicksortArray:size:"(i32* %array_ptr, i32 10)
  %a10 = load i32** @a, align 8
  %val_ptr = load i32* %a10, align 4
  %0 = getelementptr i32* %a10, i64 1
  %val_ptr11 = load i32* %0, align 4
  %1 = getelementptr i32* %a10, i64 2
  %val_ptr12 = load i32* %1, align 4
  %2 = getelementptr i32* %a10, i64 3
  %val_ptr13 = load i32* %2, align 4
  %3 = getelementptr i32* %a10, i64 4
  %val_ptr14 = load i32* %3, align 4
  %4 = getelementptr i32* %a10, i64 5
  %val_ptr15 = load i32* %4, align 4
  %5 = getelementptr i32* %a10, i64 6
  %val_ptr16 = load i32* %5, align 4
  %6 = getelementptr i32* %a10, i64 7
  %val_ptr17 = load i32* %6, align 4
  %7 = getelementptr i32* %a10, i64 8
  %val_ptr18 = load i32* %7, align 4
  %8 = getelementptr i32* %a10, i64 9
  %val_ptr19 = load i32* %8, align 4
  %printf = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([34 x i8]* @format, i64 0, i64 0), i32 %val_ptr, i32 %val_ptr11, i32 %val_ptr12, i32 %val_ptr13, i32 %val_ptr14, i32 %val_ptr15, i32 %val_ptr16, i32 %val_ptr17, i32 %val_ptr18, i32 %val_ptr19)
  ret i32 0
}
