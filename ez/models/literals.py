from ez.models.base import ASTObj


class Literal(ASTObj):
    ''' A literal. '''

    def __init__(self, token):
        super(Literal, self).__init__(token)

    def type_did_updated(self, type_obj):
        self._set_obj_type(type_obj)


class Numeric(Literal):
    ''' A numeric literal. '''

    def __init__(self, token):
        super(Numeric, self).__init__(token)
        self.value = 0


class Integer(Numeric):
    ''' An integer literal. '''

    def __init__(self, token, value):
        super(Integer, self).__init__(token)

        self.value = value

    def __repr__(self):
        return repr(self.value)


class Float(Numeric):
    ''' A floating-point literal. '''

    def __init__(self, token, value):
        super(Float, self).__init__(token)

        self.value = value

    def __repr__(self):
        return repr(self.value)


class Boolean(Literal):
    ''' A boolean literal.'''

    def __init__(self, token, value):
        super(Boolean, self).__init__(token)

        self.value = value

    def __repr__(self):
        return repr(self.value)


class Void(Literal):
    ''' A void literal (no value). '''
    
    def __init__(self, token):
        super(Void, self).__init__(token)

    def __repr__(self):
        return 'void'
