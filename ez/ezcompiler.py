import traceback

from ez.errors.ezerrormanager import EZErrorManager
from ez.tokenizer.eztokenizer import EZTokenizer
from ez.parser.ezparser import EZParser
from ez.semantic.ezsemantic import EZSemanticChecker
from ez.semantic.error import EZSemanticError
from ez.llvm.llvmgenerator import EZLlvmGenerator
from ez.visitors.printer import PrettyPrintVisitor


class EZCompiler(object):

    def __init__(self, filepath, color=True, optimize=True,
                 errors_context_line=3):
        super(EZCompiler, self).__init__()

        self.filepath = filepath

        try:
            with open(filepath, 'r') as f:
                self.content = f.read() + '\nEOF'

        except IOError, e:
            print e
            exit(1)

        self.error_manager = \
            EZErrorManager.manager(content=self.content,
                                   color=color,
                                   errors_context_line=errors_context_line)

        self.tokenizer = \
            EZTokenizer.tokenizer(error_manager=self.error_manager)

        self.parser = \
            EZParser.parser(lexer=self.tokenizer,
                            error_manager=self.error_manager)

        self.semantic_checker = \
            EZSemanticChecker.checker(error_manager=self.error_manager)

        self.llvmgenerator = \
            EZLlvmGenerator.generator(error_manager=self.error_manager,
                                      optimize=optimize)

    def tokenize(self):

        self.tokenizer.tokenize(self.content)
        self.error_manager.print_errors()

    def parse(self):

        ast_tree = self.parser.parse(self.content)
        ast_tree.accept(PrettyPrintVisitor())

        self.error_manager.print_errors()

    def check_semantic(self):

        ast_tree = self.parser.parse(self.content)

        self.semantic_checker.check(ast_tree)

        ast_tree.accept(PrettyPrintVisitor())

    def generate_llvm(self, output_filepath):

        try:
            ast_tree = self.parser.parse(self.content)

            self.semantic_checker.check(ast_tree)

            if self.error_manager.has_error():
                raise Exception('Semantic verification failed')

            module = self.llvmgenerator.generate(ast_tree)

            with open(output_filepath, 'w+') as f:
                f.write(str(module))

        except EZSemanticError, e:

            self.error_manager.add_error(e)
            self.error_manager.print_errors()
            exit(1)

        except Exception, e:

            self.error_manager.print_errors()

            traceback.print_exc()
            raise e
            exit(1)

    def compile(self, output_filepath):

        try:
            self.generate_llvm(output_filepath)

        except Exception, e:

            self.error_manager.print_errors()

            print e
            #
            #traceback.print_exc()


        #self.generate_llvm(output_filepath)
