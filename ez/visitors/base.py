from ez.util import visit as v


class Visitor(object):
    ''' A base visitor. '''
    def __init__(self):
        super(Visitor, self).__init__()

    @v.on('node')
    def visit(self, node):
        '''This is the generic method that initializes the dynamic dispatcher.
        '''

    @v.on('parent')
    def will_accept_children(self, parent):
        pass

    @v.when(object)
    def will_accept_children(self, parent):
        pass

    @v.on('parent')
    def did_accept_children(self, parent):
        pass

    @v.when(object)
    def did_accept_children(self, parent):
        pass


class ChildrenFirstVisitor(Visitor):
    ''' A visitor to visit children before the parent. '''
    pass


class ParentFirstVisitor(Visitor):
    ''' A visitor to visit the parent before the children. '''
    pass
