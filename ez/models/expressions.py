from ez.models.literals import Numeric
from ez.models.types import NumericType, FloatType, BooleanType
from ez.models.base import ASTObj, ASTError
from ez.semantic.error import EZSemanticError


class Power(ASTObj):
    '''
    A power expression.

    base ^ exponent
    '''

    def __init__(self, token, base, exponent):
        super(Power, self).__init__(token)

        self.base = base
        self.exponent = exponent

    def __repr__(self):
        return '{}^{}'.format(self.base, self.exponent)

    def accept_children(self, visitor):
        ''' Tells a visitor to visit the children. '''

        self.base.accept(visitor)
        self.exponent.accept(visitor)

    def _compute_type(self, observable):
        '''
        Compute the type of the current node according to the object type of
        the observable.
        '''
        self.check_type(self.base.obj_type, NumericType)
        self.check_type(self.base.obj_type, NumericType)

        self._set_obj_type(FloatType())


class UnaryExpr(ASTObj):
    '''
    A unary expression.

    operator value
    '''

    def __init__(self, token, operator, value):
        super(UnaryExpr, self).__init__(token)

        self.operator = operator
        self.value = value

    def __repr__(self):
        return '{}{}'.format(self.operator, self.value)

    def accept_children(self, visitor):
        ''' Tells a visitor to visit the children. '''
        self.value.accept(visitor)

    def _compute_type(self, observable):
        '''
        Compute the type of the current node according to the object type of
        the observable.
        '''
        self.check_type(self.value.obj_type, NumericType)

        self._set_obj_type(self.value.obj_type)


class BinaryExpr(ASTObj):
    '''
    A binary expression.
    
    a operator b
    '''

    def __init__(self, token, a, operator, b):
        super(BinaryExpr, self).__init__(token)

        if ((operator == '/' or operator == '//') and
                isinstance(b, Numeric) and b.value == 0):
            raise EZSemanticError(self, DivideByZeroError())

        self.a = a
        self.operator = operator
        self.b = b

    def __repr__(self):
        return '{}{}{}'.format(self.a, self.operator, self.b)

    def accept_children(self, visitor):
        ''' Tells a visitor to visit the children. '''
        self.a.accept(visitor)
        self.b.accept(visitor)

    def _compute_type(self, observable):
        '''
        Compute the type of the current node according to the object type of
        the observable.
        '''
        self.check_type(self.a.obj_type, NumericType)
        self.check_type(self.b.obj_type, NumericType)

        if (isinstance(self.a.obj_type, FloatType) or
                isinstance(self.b.obj_type, FloatType)):
            self._set_obj_type(FloatType())
        else:
            self._set_obj_type(self.a.obj_type)


class Comparison(ASTObj):
    '''
    A comparison expression.
    
    operands operators
    '''

    def __init__(self, token, operands, operators):
        super(Comparison, self).__init__(token)

        self.operands = operands
        self.operators = operators

        assert(len(self.operands) == len(self.operators) + 1)

    def __repr__(self):
        s = ''
        for i in range(0, len(self.operands) - 1):
            s += '{} {} '.format(self.operands[i], self.operators[i])

        s += '{}'.format(self.operands[len(self.operands) - 1])
        return s

    def accept_children(self, visitor):
        ''' Tells a visitor to visit the children. '''
        for op in self.operands:
            op.accept(visitor)

    def _compute_type(self, observable):
        '''
        Compute the type of the current node according to the object type of
        the observable.
        '''
        for operand in self.operands:
            self.check_type(operand.obj_type, NumericType)

        self._set_obj_type(BooleanType())

class IfElseExpr(ASTObj):
    '''
    An ... if ... else ... statement.

    a if b else c
    '''

    def __init__(self, token, a, b, c):
        super(IfElseExpr, self).__init__(token)

        self.a = a
        self.b = b
        self.c = c

    def __repr__(self):
        return '{} if {} else {}'.format(self.a, self.b, self.c)

    def accept_children(self, visitor):
        ''' Tells a visitor to visit the children. '''
        self.a.accept(visitor)
        self.b.accept(visitor)
        self.c.accept(visitor)

    def _compute_type(self, observable):
        '''
        Compute the type of the current node according to the object type of
        the observable.
        '''
        self.check_type(self.b.obj_type, BooleanType)
        self.check_type(self.c.obj_type, self.a.obj_type)

        self._set_obj_type(self.a.obj_type)




class OrTest(ASTObj):
    '''
    A or test.

    a or b
    '''

    def __init__(self, token, a, b):
        super(OrTest, self).__init__(token)

        self.a = a
        self.b = b

    def __repr__(self):
        return '{} or {}'.format(self.a, self.b)

    def accept_children(self, visitor):
        ''' Tells a visitor to visit the children. '''
        self.a.accept(visitor)
        self.b.accept(visitor)

    def _compute_type(self, observable):
        '''
        Compute the type of the current node according to the object type of
        the observable.
        '''
        self.check_type(self.a.obj_type, BooleanType)
        self.check_type(self.b.obj_type, BooleanType)

        self._set_obj_type(BooleanType())


class AndTest(ASTObj):
    '''
    A and test.

    a and b
    '''

    def __init__(self, token, a, b):
        super(AndTest, self).__init__(token)

        self.a = a
        self.b = b

    def __repr__(self):
        return '{} and {}'.format(self.a, self.b)

    def accept_children(self, visitor):
        ''' Tells a visitor to visit the children. '''
        self.a.accept(visitor)
        self.b.accept(visitor)

    def _compute_type(self, observable):
        '''
        Compute the type of the current node according to the object type of
        the observable.
        '''
        self.check_type(self.a.obj_type, BooleanType)
        self.check_type(self.b.obj_type, BooleanType)

        self._set_obj_type(BooleanType())


class NotTest(ASTObj):
    '''
    A not test.

    not a
    '''

    def __init__(self, token, a):
        super(NotTest, self).__init__(token)

        self.a = a

    def __repr__(self):
        return 'not {}'.format(self.a)

    def accept_children(self, visitor):
        ''' Tells a visitor to visit the children. '''
        self.a.accept(visitor)

    def _compute_type(self, observable):
        '''
        Compute the type of the current node according to the object type of
        the observable.
        '''
        self.check_type(self.a.obj_type, BooleanType)

        self._set_obj_type(BooleanType())


class DivideByZeroError(ASTError):
    ''' An error raised when we try to divide by zero. '''
    
    def __init__(self):
        super(ASTError, self).__init__('Unable to divide by zero.')
