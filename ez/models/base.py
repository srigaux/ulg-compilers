from ez.models.types import TypeDelegate, IntType, ObjTypeError
from ez.visitors.base import ChildrenFirstVisitor
from ez.semantic.error import EZSemanticError

class ASTObj(TypeDelegate):
    ''' A base class for the AST nodes '''

    def __init__(self, source):
        super(ASTObj, self).__init__()

        assert(source is not None)

        if isinstance(source, tuple):
            self.source_range = source
        else:
            token = source.slice[0]
            self.source_range = (token.lexpos, token.endlexpos)

        self._type = self.__class__.__name__
        self.symbols = None
        self.obj_type = None
        self._observers = []
        self._dependencies = []

    def accept(self, visitor):
        '''
        Tells the visitor to visit the current node.
        Visits the children first or after having visited the current node
        according to the type of visitor.
        '''
        if isinstance(visitor, ChildrenFirstVisitor):
            self._accept_children(visitor)
            visitor.visit(self)

        else:
            visitor.visit(self)
            self._accept_children(visitor)

    def _accept_children(self, visitor):
        '''
        Tells the visitor that we will visit the children of the current
        node. Then actually do it. And finally tells the visitor that the
        children of the current node have been visited.
        '''
        visitor.will_accept_children(self)
        self.accept_children(visitor)
        visitor.did_accept_children(self)

    def accept_children(self, visitor):
        ''' Tells a visitor to visit the children. '''
        pass

    def _set_dependencies(self, dependencies):
        '''
        Adds dependencies to the current node and register as observer to those
        dependencies.
        '''
        self._dependencies = dependencies

        for dependency in dependencies:
            dependency._observers.append(self)

    def _add_dependency(self, dependency):
        '''
        Adds a dependency to the current node and register as observer to this
        dependency.
        '''
        self._dependencies.append(dependency)
        dependency._observers.append(self)

    def _set_obj_type(self, obj_type):
        '''
        Initializes the object type of the current node and tells all the
        observer to compute their types if all the types of their dependencies
        have already been checked.
        '''
        if obj_type is None:
            return

        if self.obj_type == obj_type:
            return

        if self.obj_type is not None:
            raise EZSemanticError(self,
                                  ObjTypeError(obj_type,
                                               self.obj_type))

        self.obj_type = obj_type

        for observer in self._observers:
            if observer._check_dependencies_types():
                observer._compute_type(self)

    def _compute_type(self, observable):
        '''
        Compute the type of the current node according to the object type of
        the observable.
        '''
        raise NotImplementedError(
            self._type + " : def _compute_type(self, observable)")

    def _check_dependencies_types(self):
        '''
        Returns True if the types of all dependencies have all been checked.
        Otherwise,returns False.
        '''
        for dep in self._dependencies:
            if dep.obj_type is None:
                return False

        return True

    def repr_dict(self):
        ''' Builds a dict to base on to represent the current node. '''
        exclude = \
            [
                'symbols',
                '_observers',
                '_dependencies',
                'inner_type',
            ]

        dict = \
            {k: self.__dict__[k] for k in self.__dict__
                if k not in exclude}

        dict['_id'] = '0x' + str(id(self))
        dict['DEP'] = ' | '.join([dep._type + ' 0x' + str(id(dep))
                                 for dep
                                 in self._dependencies])

        return dict

    def check_type(self, expected, given):
        ''' Checks if the given type is equal to the expected one. '''
        if isinstance(expected, ASTObj):
            self.check_type(expected.obj_type, given)
        elif not isinstance(given, type):
            self.check_type(expected, given.__class__)
        else:
            if not isinstance(expected, given):
                raise EZSemanticError(self, ObjTypeError(expected, given))


class ASTError(Exception):
    ''' An error relative to the AST. '''
    pass


class Program(ASTObj):
    '''
    The entry point of a program.
    Contains a list of statements.
    '''

    def __init__(self, token, statements):
        super(Program, self).__init__(token)

        from ez.models.functions import FunctionDefinition
        from ez.models.compound_statements import StatementList

        # Split the function from the other instruction and pack the laters in a
        # main function
        main_stmts = []
        func_defs = []

        for stmt in statements:
            if isinstance(stmt, FunctionDefinition):
                func_defs.append(stmt)
            else:
                main_stmts.append(stmt)

        main_body = StatementList(token, main_stmts)
        main = FunctionDefinition(token, 'main', [], main_body)
        main.obj_type = IntType()

        func_defs.append(main)

        self.statements = func_defs
        self.main_function = main

    def __repr__(self):
        return repr(self.statements)

    def accept_children(self, visitor):
        ''' Tells a visitor to visit the children. '''

        for statement in self.statements:
            statement.accept(visitor)

    def type_did_updated(self, obj_type):
        ''' Tells the current node that its type is known. '''
        self._set_obj_type(obj_type)

    def _compute_type(self, observable):
        '''
        Compute the type of the current node according to the object type of
        the observable.
        '''
        pass


class UniqueIdentifier(ASTObj):
    ''' An identifier that is used to bind multiple related identifiers. '''

    instances = {}

    def __repr__(self):
        return '0x' + str(id(self))

    def repr_dict(self):
        ''' Builds a dict to base on to represent the current node. '''
        return str(self)

    def _compute_type(self, observable):
        '''
        Compute the type of the current node according to the object type of
        the observable.
        '''
        self._set_obj_type(observable.obj_type)

    def _check_dependencies_types(self):
        '''
        Returns True if the types of all dependencies have all been checked.
        Otherwise,returns False.
        '''
        return True


class Identifier(ASTObj):
    ''' An identifier. '''

    def __init__(self, token, name):
        super(Identifier, self).__init__(token)

        self.name = name
        self.scope_name = None
        self.is_assignment = False
        self.uid = None

    def scoped_name(self):
        ''' The full name of the identifier with its scope name '''
        if self.scope_name is None:
            return self.name
        else:
            return '{}_#_{}'.format(self.scope_name,
                                    self.name)

    def __repr__(self):
        return self.name


    def accept_children(self, visitor):
        ''' Tells a visitor to visit the children. '''

        if self.uid is not None:
            self.uid.accept(visitor)

    def _compute_type(self, observable):
        '''
        Compute the type of the current node according to the object type of
        the observable.
        '''
        self._set_obj_type(observable.obj_type)

    def _set_obj_type(self, obj_type):
        '''
        Sets the object type of the identifier and tells it to the associated
        unique identifier.
        '''
        super(Identifier, self)._set_obj_type(obj_type)
        self.uid._compute_type(self)

    def _check_dependencies_types(self):
        '''
        Returns True if the types of all dependencies have all been checked.
        Otherwise,returns False.
        '''
        for dep in self._dependencies:
            if dep.obj_type is None and\
                    not isinstance(dep, UniqueIdentifier):
                return False

        return True
