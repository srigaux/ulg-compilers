'''
rules.py

Rules for the tokenizer of the EZ language.
'''


states = (
    ('indent', 'exclusive'),
    ('comment', 'exclusive'),
)

# List of reserved tokens
reserved = {
    'while'     : 'WHILE',
    'break'     : 'BREAK',
    'continue'  : 'CONTINUE',
    'for'       : 'FOR',
    'to'        : 'TO',
    'if'        : 'IF',
    'else'      : 'ELSE',
    'function'  : 'FUNCTION',
    'return'    : 'RETURN',
    'array'     : 'ARRAY',
    'or'        : 'OR',
    'and'       : 'AND',
    'not'       : 'NOT',
    'Yes'       : 'YES',
    'No'        : 'NO',
    'print'     : 'PRINT',
    'EOF'       : 'EOF'
}

# List of token names
tokens = [
    'FLOAT',
    'INTEGER',

    'PLUS',
    'MINUS',
    'TIMES',
    'DIVIDE',
    'DIVINT',
    'EXP',
    'MOD',

    'EQUAL',

    'AUGOPPLUS',
    'AUGOPMINUS',
    'AUGOPTIMES',
    'AUGOPDIVIDE',
    'AUGOPDIVINT',
    'AUGOPEXP',
    'AUGOPMOD',


    'EQ',
    'NEQ',
    'GT',
    'LT',
    'GTE',
    'LTE',


    'LPAREN',
    'RPAREN',
    'LSQUARE',
    'RSQUARE',
    'LBRAQU',
    'RBRAQU',

    'COMMA',
    'COLON',
    'SEMICOLON',

    'ID',

    'NEWLINE',
    'INDENT',
    'DEDENT',

] + list(reserved.values())

# Token rules for arithmetic operators
t_PLUS = r'\+'
t_MINUS = r'-'
t_TIMES = r'\*'
t_DIVIDE = r'/'
t_DIVINT = r'//'
t_EXP = r'\^'
t_MOD = r'%'

# Token rules for assignment operators
t_EQUAL = r'='

t_AUGOPPLUS = r'\+='
t_AUGOPMINUS = r'-='
t_AUGOPTIMES = r'\*='
t_AUGOPDIVIDE = r'/='
t_AUGOPDIVINT = r'//='
t_AUGOPEXP = r'\^='
t_AUGOPMOD = r'%='

# Token rules for comparison operators
t_EQ = r'=='
t_NEQ = r'(!=)|(<>)'
t_GT = r'>'
t_LT = r'<'
t_GTE = r'(>=)|(=>)'
t_LTE = r'(<=)|(=<)'

# Token rules for parenthesis and brackets
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_LSQUARE = r'\['
t_RSQUARE = r'\]'
t_LBRAQU = r'\{'
t_RBRAQU = r'\}'

# Token rules for comma, colon and semicolon
t_COMMA = r','
t_COLON = r':'
t_SEMICOLON = r';'

# Token rules for ignored characters (whitespace)
t_ignore = ' \t'
t_indent_ignore = ''    # TODO: Remove ?
t_comment_ignore = ''   # TODO: Remove ?


def t_ID(t):
    r'[A-Za-z_][_0-9A-Za-z]*'
    ''' Matches an identifier and build a token for it. '''
    t.type = reserved.get(t.value, 'ID')
    return t


def t_FLOAT(t):
    r'([1-9]\d*\.\d*)|(0?\.\d*)'
    ''' Matches a floating point value and build a token for it. '''
    t.value = float(t.value)
    return t


def t_INTEGER(t):
    r'([1-9]\d*)|0'
    ''' Matches an integer value and build a token for it. '''
    t.value = int(t.value)
    return t


def t_MULTILINECOMMENT(t):
    r'\#\*'
    '''
    Matches an opening of a comment on multiple lines, returns 2 characters
    back and switches to 'comment' state.
    '''
    t.lexer.lexpos -= 2
    t.lexer.begin('comment')


def t_INLINECOMMENT(t):
    r'[\t\ ]*\#(.*?)\n'
    ''' Comment on one line '''
    t.type = 'COMMENT'
    t.value = t.value[1:-1]

    t.lexer.lexpos -= 1


def t_newline(t):
    r'(\s*\n)+'
    '''
    Matches a new line after a non-empty line, builds a token for it and
    switches to 'indent' state.
    '''
    t.lexer.lineno += len(t.value)
    t.type = 'NEWLINE'
    t.value = ''
    t.lexer.begin('indent')
    return t


def t_FIRSTLINE(t):
    r'\n+'
    '''
    Matches (escapes) multiple lines at the beginning of the input file.
    '''


def t_ANY_error(t):
    ''' Handles an error. '''
    t.lexer.add_error(t)
    t.lexer.skip(1)


#------------------- Rules of the 'indent' state -----------------------


def t_indent_NOTAB(t):
    r'[^\t\ ]'
    '''
    Matches any character that is not a tab. This is used to build DEDENT tokens
    if we came back to the level 0 of indentation.
    '''

    i = len(t.value)
    t.lexer.lexpos -= i

    if (t.lexer.indent > 0):
        t.type = 'DEDENT'
        t.lexer.indent -= 1
        t.value = t.lexer.indent
        return t
    else:
        t.lexer.begin('INITIAL')


def t_indent_TAB(t):
    r'(\ |\t)+'
    '''
    Matches tabulations/spaces and builds an INDENT or DEDENT token.
    '''

    i = len(t.value)
    j = 0

    # Count the number of spaces and count 4 spaces per tabulation.
    for c in t.value:
        j += 4 if c == '\t' else 1

    # Raise a warning if the number of spaces is not a multiple of 4
    if(j % 4 != 0):

        offset = (j / 4) * 4
        t.lexpos += offset
        t.value = t.value[offset:]

        t.lexer.add_error(t,
                          'Probably wrong indentation',
                          'The highlighted characters have been removed.',
                          is_warning=True)
        return

    # Compute the indentation level
    j /= 4

    # If it is greater than the current one, then build an INDENT token
    if (t.lexer.indent < j):
        t.lexer.indent += 1
        t.value = t.lexer.indent
        t.type = 'INDENT'
        t.lexer.lexpos -= i
        return t

    # If it is less than the current one, then build a DEDENt token
    elif (t.lexer.indent > j):
        t.lexer.indent -= 1
        t.value = t.lexer.indent
        t.type = 'DEDENT'
        t.lexer.lexpos -= i
        return t

    # Go back to the 'INITIAL' state
    t.lexer.begin('INITIAL')


#---------------------- comment state ----------------------


def t_comment_NESTING(t):
    r'\#\*'
    '''
    Matches a multi-line comment opening inside another multi-line comment and
    increases the counter of nested multi-line comments.
    '''

    t.lexer.comment += t.value
    t.lexer.comment_depth += 1


def t_comment_UNNESTING(t):
    r'\*\#'
    '''
    Matches a multi-line comment closing inside another multi-line comment,
    decreases the counter of nested multi-line comment and  if this counter
    reaches 0, then go back to the 'INITIAL' state.
    '''

    t.lexer.comment_depth -= 1
    t.lexer.comment += t.value

    if t.lexer.comment_depth == 0:
        t.type = 'COMMENT'
        t.value = t.lexer.comment[2:-2]

        t.lexer.comment = ""
        t.lexer.begin('INITIAL')



def t_comment_CONTENT(t):
    r'[^\*\#]+'
    '''
    Matches (ignore) everything that is not a closing multi-line comment and
    stores the characters into the lexer.
    '''
    t.lexer.comment += t.value


def t_comment_CONTENT2(t):
    r'[\*\#]+'
    '''
    Matches * and # characters.
    '''
    t.lexer.comment += t.value

def t_INITIAL_UNNESTING(t):
    r'\*\#'
    '''
    Matches a closing multi-line comment that has not been opened. 
    '''
    t.lexer.add_error(t, message='Unexpected closing multiline comment token')
