from ez.models.base import *
from ez.models.arrays import *
from ez.models.compound_statements import *
from ez.models.expressions import *
from ez.models.functions import *
from ez.models.literals import *
from ez.models.simple_statements import *
from ez.models.types import ContainerType

from ez.util import visit as v
from ez.visitors.base import *


class UninitializedVisitor(ChildrenFirstVisitor):

    def __init__(self, error_manager):
        super(UninitializedVisitor, self).__init__()

        self.uninitialized = []
        self.error_manager = error_manager
        self.raise_error = False
        self.skip = False

    @v.on('node')
    def visit(self, node):
        '''This is the generic method that initializes the dynamic dispatcher.
        '''

    @v.on('parent')
    def will_accept_children(self, parent):
        pass

    @v.when(ASTObj)
    def will_accept_children(self, parent):
        self.raise_error = False


    @v.when(FunctionDefinition)
    def will_accept_children(self, parent):
        self.skip = True


    @v.on('parent')
    def did_accept_children(self, parent):
        pass

    @v.when(ASTObj)
    def did_accept_children(self, parent):
        pass

    @v.when(FunctionDefinition)
    def did_accept_children(self, parent):
        self.skip = False

    # -------------------------------------------------------------------------
    #                              BASE
    # -------------------------------------------------------------------------

    @v.when(ASTObj)
    def visit(self, node):
        ''' ASTObj '''

        if not self.raise_error and not self.skip:

            if (node.obj_type is None or

                    (isinstance(node.obj_type, ContainerType) and
                     node.obj_type.inner_type is None)):

                self.raise_error = True

                msg = 'The type cannot be found ({})'.format(str(node))
                self.error_manager.add_error(EZSemanticError(node,
                                                             msg
                                                             ))
            else:
                self.raise_error = False

    @v.when(ArrayAccess)
    def visit(self, node):
        pass

    @v.when(UniqueIdentifier)
    def visit(self, node):
        pass

    @v.when(AssignmentStatement)
    def visit(self, node):
        pass

    @v.when(StatementList)
    def visit(self, node):

        if not self.raise_error and not self.skip:

            if node.obj_type is None and node.has_return:

                self.raise_error = True

                msg = 'The type cannot be found ({})'.format(str(node))
                self.error_manager.add_error(EZSemanticError(node,
                                                             msg
                                                             ))
            else:
                self.raise_error = False

    @v.when(IfStatement)
    def visit(self, node):
        pass


    @v.when(ForStatement)
    def visit(self, node):
        pass

    @v.when(FunctionDefinition)
    def visit(self, node):
        pass
