from llvm.core import Type as LlvmType

class TypeDelegate(object):
    ''' An 'interface' that enables to plug as a delegate to a type.  '''
    def __init__(self):
        super(TypeDelegate, self).__init__()

    def type_did_updated(self, type_obj):
        raise NotImplementedError(type_obj)


class Type(object):
    ''' A base class for the types. '''
    instances = {}

    def __new__(cls, *args, **kargs):

        identifier = cls._identifier(args, len(Type.instances))

        instance = Type.instances.get(identifier)

        if instance is None or cls == ArrayType:
            instance = object.__new__(cls, *args, **kargs)
            instance.observers = []
            instance.propagated = False

            if cls == FunctionType:
                instance.args_types = None
                instance.name = args[0]
                instance.args_count = args[1]
                instance._definition = None
                instance._definition_observers = []

            Type.instances[identifier] = instance

        return instance

    def __cmp__(self, other):
        return 0 if self.__class__ == other.__class__ else 1

    def __repr__(self):
        return self.__class__.__name__

    def repr_dict(self):
        ''' Builds a dict to base on to represent the current object. '''
        return repr(self)

    def llvm_type(self):
        ''' Return the LLVM type of the current (EZ) type. '''
        return None

    def add_observer(self, observer):
        ''' Add an observer to the current node. '''
        self.observers.append(observer)

    def signal_observers(self):
        '''
        Signal all the observers that they are of the type of the current
        object.
        '''
        self.propagated = True
        for observer in self.observers:
            observer.type_did_updated(self)


class VoidType(Type):
    ''' A void type. '''

    @staticmethod
    def _identifier(args, i):
        ''' Returns an identifier for the type. '''
        return 'void'

    def llvm_type(self):
        ''' Return the LLVM type of the current (EZ) type. '''
        return LlvmType.void()


class NumericType(Type):
    ''' A numeric type. '''

    @staticmethod
    def get_bigest_type(types):
        for t in types:
            if isinstance(t, FloatType):
                return FloatType

        return IntType

    @staticmethod
    def cast(value, source_type_inst, destination_type):
        ''' Casts a numeric type from a source type to a destination type. '''

        source_type = type(source_type_inst)

        if source_type == destination_type:
            return value

        if source_type == IntType and destination_type == FloatType:
            return Semantic.builder.sitofp(value, LlvmType.float(), 'floattmp')

        if source_type == FloatType and destination_type == IntType:
            return Semantic.builder.fptosi(value, LlvmType.int(), 'inttmp')

        raise RuntimeError('NumericType : cast : Not expected type {}'
                           .format([source_type, destination_type]))


class IntType(NumericType):
    ''' An integer type. '''

    @staticmethod
    def _identifier(args, i):
        ''' Returns an identifier for the type. '''
        return 'int'

    def llvm_type(self):
        ''' Return the LLVM type of the current (EZ) type. '''
        return LlvmType.int(32)


class FloatType(NumericType):
    ''' A floating-point type. '''

    @staticmethod
    def _identifier(args, i):
        ''' Returns an identifier for the type. '''
        return 'float'

    def llvm_type(self):
        ''' Return the LLVM type of the current (EZ) type. '''
        return LlvmType.double()


class BooleanType(Type):
    ''' A boolean type. '''

    @staticmethod
    def _identifier(args, i):
        ''' Returns an identifier for the type. '''
        return 'boolean'

    def llvm_type(self):
        ''' Return the LLVM type of the current (EZ) type. '''
        return LlvmType.int(1)


class ContainerType(Type):
    ''' A type that contains an inner type. '''

    def __init__(self, inner_type=None):
        super(ContainerType, self).__init__()

        self.inner_type = inner_type
        self.inner_type_observers = []

    def __cmp__(self, other):

        if type(self) != type(other):
            return 1

        if id(self) == id(other):
            return 0

        if self.inner_type != other.inner_type:
            return 1

        return 0

    def add_inner_type_observer(self, observer):
        ''' Add an observer that watches the inner type. '''
        self.inner_type_observers.append(observer)

        if self.inner_type is not None:
            self.signal_inner_type_observer(observer)

    def set_inner_type(self, inner_type):
        ''' Sets the inner type. '''
        if self.inner_type == inner_type:
            return

        if self.inner_type is not None:
            raise ObjTypeError(self.inner_type, inner_type)

        self.inner_type = inner_type
        self.signal_all_inner_type_observers()

    def signal_all_inner_type_observers(self):
        ''' Signals the observers that the inner type has been initialized. '''
        for observer in self.inner_type_observers:
            self.signal_inner_type_observer(observer)

    def signal_inner_type_observer(self, observer):
        ''' Signals an observer that the inner type has been initialized. '''
        observer.inner_type_did_updated(self)


class ArrayType(ContainerType):
    ''' An array type. '''

    @staticmethod
    def _identifier(args, i):
        ''' Returns an identifier for the type. '''
        return 'array' + str(i)

    def __init__(self, size, inner_type=None):
        super(ArrayType, self).__init__(inner_type)

        self.size = size

    def __cmp__(self, other):

        if super(ArrayType, self).__cmp__(other) == 1:
            return 1

        if self.size != other.size:
            return 1

        return 0

    def __repr__(self):
        return 'ArrayType({})[{}]'.format(self.inner_type, self.size)

    def llvm_type(self):
        ''' Return the LLVM type of the current (EZ) type. '''
        inner_llvm_type = self.inner_type.llvm_type()
        return LlvmType.array(inner_llvm_type, self.size)


class PointerType(ContainerType):
    ''' A pointer type. '''

    @staticmethod
    def _identifier(args, i):
        ''' Returns an identifier for the type. '''
        return 'pointer' + str(i)

    def __init__(self, inner_type, from_type=None):
        super(PointerType, self).__init__(inner_type)

        self.set_inner_type(inner_type)
        self.from_type = from_type

        if isinstance(from_type, ContainerType):
            from_type.add_inner_type_observer(self)

    def __repr__(self):
        return "*{}".format(self.inner_type)

    def llvm_type(self):
        ''' Return the LLVM type of the current (EZ) type. '''
        inner_llvm_type = self.inner_type.llvm_type()
        return LlvmType.pointer(inner_llvm_type)

    def inner_type_did_updated(self, container):
        self.set_inner_type(container.inner_type)

class FunctionType(Type):
    ''' A function type. '''

    @staticmethod
    def _identifier(args, i):
        ''' Returns an identifier for the type. '''
        return 'function_' + args[0]

    def __init__(self, name, args_count):
        super(Type, self).__init__()

        if self.name == 'main':
            self.is_used = True
        else:
            self.is_used = False

    def __cmp__(self, other):

        if type(self) != type(other):
            return 1

        if self.name != other.name:
            return 1

        if len(self.args_types) != len(other.args_types):
            return 1

        return 0

    def __repr__(self):
        name = self.name

        return 'FunctionType({})'.format(name)

    def add_definition_observer(self, observer):
        ''' Adds an observer that watches the definition. '''
        self._definition_observers.append(observer)

        if self._definition is not None:
            observer.definition_did_updated(self._definition)


    def set_definition(self, definition):
        '''
        Sets the definition of the associated to the current FunctionType and
        tells all the observers that the definition has been set.
        '''
        self._definition = definition

        for obs in self._definition_observers:
            obs.definition_did_updated(self._definition)

    def check_args_types(self, args_types):
        ''' Check the number and the types of the arguments. '''
        if self.args_types == args_types:
            return

        assert self.args_count == len(args_types)

        self.args_types = [None for i in range(self.args_count)]

        for i in range(len(args_types)):

            if self.args_types[i] is None:
                self.args_types[i] = args_types[i]
            else:
                raise ObjTypeError(self.args_types[i], args_types[i])

        #self.did_updated()

    def is_undefined(self):
        return self.args_types is None

    def did_updated(self):
        ''' Signals the delegate. '''
        if self.delegate is not None:
            self.delegate.type_did_updated(self)


class ObjTypeError(Exception):
    '''
    An error that is raised when a given type doesn't math the expected one.
    '''

    def __init__(self, given_type, expected_type):

        message = \
            "Types don't match : given '{}', expected '{}'" \
            .format(given_type, expected_type)

        super(ObjTypeError, self).__init__(message)

        self.given_type = given_type
        self.expected_type = expected_type


class UndeterminatedTypeError(Exception):
    ''' An error that is raised when we encounter an undeterminated type. '''
    pass
