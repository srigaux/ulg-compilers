from ez.models.base import *
from ez.models.arrays import *
from ez.models.compound_statements import *
from ez.models.expressions import *
from ez.models.functions import *
from ez.models.literals import *
from ez.models.simple_statements import *
from ez.models.types import *


from ez.util import visit as v
from ez.visitors.base import ParentFirstVisitor
from ez.models.arrays import ArrayInit


class DependenciesVisitor(ParentFirstVisitor):
    '''
    A visitor that will set up the dependencies between all the AST nodes.
    '''

    @v.on('node')
    def visit(self, node):
        '''This is the generic method that initializes the dynamic dispatcher.
        '''

    @v.on('parent')
    def did_accept_children(self, parent):
        pass

    @v.when(Program)
    def did_accept_children(self, parent):
        for t in Type.instances.values():
            t.signal_observers()

    # -------------------------------------------------------------------------
    #                              BASE
    # -------------------------------------------------------------------------

    @v.when(ASTObj)
    def visit(self, node):
        ''' ASTObj '''
        raise NotImplementedError(str(node.__class__))

    @v.when(Program)
    def visit(self, node):
        ''' Program '''
        VoidType().add_observer(node)
        node._set_dependencies(node.statements)


    @v.when(UniqueIdentifier)
    def visit(self, node):
        ''' UniqueIdentifier '''


    @v.when(Identifier)
    def visit(self, node):
        ''' Identifier '''
        node._set_dependencies([node.uid])

    # -------------------------------------------------------------------------
    #                              LITERALS
    # -------------------------------------------------------------------------

    @v.when(Integer)
    def visit(self, node):
        ''' Integer '''
        IntType().add_observer(node)

    @v.when(Float)
    def visit(self, node):
        ''' Float '''
        FloatType().add_observer(node)

    @v.when(Boolean)
    def visit(self, node):
        ''' Boolean '''
        BooleanType().add_observer(node)

    @v.when(Void)
    def visit(self, node):
        ''' Void '''
        VoidType().add_observer(node)

    # -------------------------------------------------------------------------
    #                               ARRAYS
    # -------------------------------------------------------------------------

    @v.when(ArrayInit)
    def visit(self, node):
        ''' ArrayInit '''
        if node.values is None:
            PointerType(None,
                        ArrayType(node.length,
                                  None)).add_observer(node)
        else:
            node._set_dependencies(node.values)


    @v.when(ArrayAccess)
    def visit(self, node):
        ''' ArrayAccess '''

        node._set_dependencies([node.array_identifier,
                                node.index])

    # -------------------------------------------------------------------------
    #                              EXPRESSIONS
    # -------------------------------------------------------------------------

    @v.when(Power)
    def visit(self, node):
        ''' Power '''
        node._set_dependencies([node.base, node.exponent])

    @v.when(UnaryExpr)
    def visit(self, node):
        ''' UnaryExpr '''
        node._set_dependencies([node.value])

    @v.when(BinaryExpr)
    def visit(self, node):
        ''' BinaryExpr '''
        node._set_dependencies([node.a, node.b])

    @v.when(Comparison)
    def visit(self, node):
        ''' Comparison '''
        node._set_dependencies(node.operands)

    @v.when(IfElseExpr)
    def visit(self, node):
        ''' IfElseExpr '''
        node._set_dependencies([node.a, node.b, node.c])

    @v.when(OrTest)
    def visit(self, node):
        ''' OrTest '''
        node._set_dependencies([node.a, node.b])

    @v.when(AndTest)
    def visit(self, node):
        ''' AndTest '''
        node._set_dependencies([node.a, node.b])

    @v.when(NotTest)
    def visit(self, node):
        ''' NotTest '''
        node._set_dependencies([node.a])


    # -------------------------------------------------------------------------
    #                          SIMPLE STATEMENTS
    # -------------------------------------------------------------------------

    @v.when(AssignmentStatement)
    def visit(self, node):
        ''' AssignmentStatement '''

        dependencies = [node.value]

        for target in node.targets:
            target._set_dependencies([node])

            if isinstance(target, ArrayAccess):
                dependencies.append(target.array_identifier)

        node._set_dependencies(dependencies)

    @v.when(PrintStatement)
    def visit(self, node):
        ''' PrintStatement '''
        VoidType().add_observer(node)

    @v.when(BreakStatement)
    def visit(self, node):
        ''' BreakStatement '''

    @v.when(ContinueStatement)
    def visit(self, node):
        ''' ContinueStatement '''

    # -------------------------------------------------------------------------
    #                           COMPOUND STATEMENTS
    # -------------------------------------------------------------------------

    @v.when(StatementList)
    def visit(self, node):
        ''' StatementList '''
        node._set_dependencies(node.return_statements)

    @v.when(IfStatement)
    def visit(self, node):
        ''' IfStatement '''
        dependencies = [node.a, node.b]

        if node.c is not None:
            dependencies.append(node.c)

        node._set_dependencies(dependencies)

    @v.when(ForStatement)
    def visit(self, node):
        ''' ForStatement '''
        dependencies = [node.cond, node.body]

        if node.init is not None:
            dependencies.append(node.init)

        if node.increment is not None:
            dependencies.append(node.increment)

        node._set_dependencies(dependencies)

    # -------------------------------------------------------------------------
    #                              FUNCTIONS
    # -------------------------------------------------------------------------

    @v.when(FunctionDefinition)
    def visit(self, node):
        ''' FunctionDefinition '''

        node.function_type = FunctionType(node.name, len(node.args))
        node.function_type.set_definition(node)

        node._set_dependencies([node.body])

    @v.when(FunctionCall)
    def visit(self, node):
        ''' FunctionCall '''

        node.function_type = FunctionType(node.name, len(node.arg_values))
        node.function_type.is_used = True
        node.function_type.add_definition_observer(node)

    @v.when(ReturnStatement)
    def visit(self, node):
        ''' ReturnStatement '''
        node._set_dependencies([node.expression])
