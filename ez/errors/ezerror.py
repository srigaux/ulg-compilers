# Some color constants for ANSI/VT100 terminals.
DEFAULT = 0
BOLD = 1

GRAY = 0
RED = 1
GREEN = 2
YELLOW = 3
BLUE = 4
PURPLE = 5
CYAN = 6
WHITE = 7

FOREGROUND = 30
BACKGROUND = 40


class EZErrorLevel(object):
    ''' Stores the name and the colors of an error level. '''

    def __init__(self, level, name, title_color, source_color):
        self.level = level
        self.name = name

        title_color_str = [str(c) for c in title_color]
        source_color_str = [str(c) for c in source_color]

        self.title_color = '\033[{}m'.format(';'.join(title_color_str))
        self.source_color = '\033[{}m'.format(';'.join(source_color_str))


class EZError(Exception):
    ''' An error that may occur during the compilation. '''

    TRACE = EZErrorLevel(0, 'TRACE',
                         [FOREGROUND+GRAY],
                         [BOLD, BACKGROUND+GRAY, FOREGROUND+WHITE])

    DEBUG = EZErrorLevel(10, 'DEBUG',
                         [FOREGROUND+GREEN],
                         [BOLD, BACKGROUND+GREEN, FOREGROUND+WHITE])

    NOTICE = EZErrorLevel(11, 'NOTICE',
                         [FOREGROUND+CYAN],
                         [BOLD, FOREGROUND+CYAN])

    WARNING = EZErrorLevel(20, 'WARNING',
                           [FOREGROUND+YELLOW],
                           [BOLD, BACKGROUND+YELLOW, FOREGROUND+WHITE])

    ERROR = EZErrorLevel(30, 'ERROR',
                         [FOREGROUND+RED],
                         [BOLD, BACKGROUND+RED, FOREGROUND+WHITE])

    CRITICAL = EZErrorLevel(40, 'CRITICAL',
                            [FOREGROUND+RED],
                            [BOLD, BACKGROUND+RED, FOREGROUND+WHITE])

    def __init__(self, source, inner, level=ERROR, details=None):
        if isinstance(inner, str):
            self.inner_exception = Exception(inner)
        else:
            self.inner_exception = inner

        super(EZError, self).__init__(self.inner_exception.message)

        self.source = source
        self.level = level
        self.details = details
