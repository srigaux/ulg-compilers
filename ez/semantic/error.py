from ez.errors.ezerror import EZError


class EZSemanticError(EZError):

    ''' An error related to the semantic part of the compiler. '''
    def __init__(self, source, inner, level=EZError.ERROR, details=None):
        super(EZSemanticError, self).__init__(source, inner, level, details)
