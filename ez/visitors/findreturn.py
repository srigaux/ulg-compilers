from ez.models.arrays import *
from ez.models.base import *
from ez.models.compound_statements import *
from ez.models.expressions import *
from ez.models.functions import *
from ez.models.literals import *
from ez.models.simple_statements import *

from ez.util import visit as v
from ez.visitors.base import ParentFirstVisitor


class FindReturnVisitor(ParentFirstVisitor):
    '''
    A visitor to find the return statements of the StatementLists and to set a
    flag that says if the StatementList has a return statement in its first
    sub-scope.

    The FunctionDefinition nodes, if they have no return statement in them put
    themselves a observer of the VoidType.

    The main function observes the IntType.
    '''

    def __init__(self):
        super(FindReturnVisitor, self).__init__()

        self.return_statements = []

    @v.on('node')
    def visit(self, node):
        '''This is the generic method that initializes the dynamic dispatcher.
        '''

    @v.on('parent')
    def will_accept_children(self, parent):
        pass

    @v.on('parent')
    def did_accept_children(self, parent):
        pass


    @v.when(ASTObj)
    def visit(self, node):
        ''' ASTObj '''
        pass

    @v.when(ReturnStatement)
    def visit(self, stmt):
        ''' ReturnStatement '''

        self.return_statements.append(stmt)

    @v.when(StatementList)
    def will_accept_children(self, parent):
        self.return_statements = []


    @v.when(FunctionDefinition)
    def did_accept_children(self, function):

        stmt_list = function.body

        if not stmt_list.has_return:
            if function.name == 'main':
                IntType().add_observer(stmt_list)
            else:
                VoidType().add_observer(stmt_list)

    @v.when(StatementList)
    def did_accept_children(self, stmts):
        stmts.has_return = len(self.return_statements) != 0
        stmts.return_statements = self.return_statements
        self.return_statements = []
