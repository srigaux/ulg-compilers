from ez.errors.ezerror import EZError
from ez.visitors.llvm_generator import LLVMGeneratorVisitor


class EZLlvmGeneratorError(EZError):

    def __init__(self, source, inner, level=EZError.ERROR, details=None):
        super(EZLlvmGeneratorError, self).__init__(source, inner,
                                                   level, details)


class EZLlvmGenerator(object):

    def __init__(self, error_manager, optimize):
        super(EZLlvmGenerator, self).__init__()

        self.error_manager = error_manager
        self.optimize = optimize

    @staticmethod
    def generator(error_manager=None, optimize=True):
        return EZLlvmGenerator(error_manager, optimize)

    def generate(self, ast_tree):
        llvm_visitor = LLVMGeneratorVisitor(self.error_manager,
                                            self.optimize)
        llvm_visitor.visit(ast_tree)
        return llvm_visitor.module
