from ez.models import *
from ez.util import visit as v
from ez.visitors.base import *


class EmptyVisitor(ChildrenFirstVisitor):
    ''' An empty visitor to use as pattern to build other visitors. '''

    @v.on('node')
    def visit(self, node):
        '''This is the generic method that initializes the dynamic dispatcher.
        '''

    # -------------------------------------------------------------------------
    #                              BASE
    # -------------------------------------------------------------------------

    @v.when(ASTObj)
    def visit(self, node):
        ''' ASTObj '''
        pass

    @v.when(Program)
    def visit(self, node):
        ''' Program '''
        pass

    @v.when(Identifier)
    def visit(self, node):
        ''' Identifier '''
        pass

    # -------------------------------------------------------------------------
    #                              LITERALS
    # -------------------------------------------------------------------------

    @v.when(Numeric)
    def visit(self, node):
        ''' Numeric '''
        pass

    @v.when(Integer)
    def visit(self, node):
        ''' Integer '''
        pass

    @v.when(Float)
    def visit(self, node):
        ''' Float '''
        pass

    @v.when(Boolean)
    def visit(self, node):
        ''' Boolean '''
        pass

    @v.when(Void)
    def visit(self, node):
        ''' Void '''
        pass

    # -------------------------------------------------------------------------
    #                               ARRAYS
    # -------------------------------------------------------------------------

    @v.when(ArrayInit)
    def visit(self, node):
        ''' ArrayInit '''
        pass

    @v.when(ArrayAccess)
    def visit(self, node):
        ''' ArrayAccess '''
        pass

    # -------------------------------------------------------------------------
    #                              EXPRESSIONS
    # -------------------------------------------------------------------------

    @v.when(Power)
    def visit(self, node):
        ''' Power '''
        pass

    @v.when(UnaryExpr)
    def visit(self, node):
        ''' UnaryExpr '''
        pass

    @v.when(BinaryExpr)
    def visit(self, node):
        ''' BinaryExpr '''
        pass

    @v.when(Comparison)
    def visit(self, node):
        ''' Comparison '''
        pass

    @v.when(IfElseExpr)
    def visit(self, node):
        ''' IfElseExpr '''
        pass

    @v.when(OrTest)
    def visit(self, node):
        ''' OrTest '''
        pass

    @v.when(AndTest)
    def visit(self, node):
        ''' AndTest '''
        pass

    @v.when(NotTest)
    def visit(self, node):
        ''' NotTest '''
        pass

    # -------------------------------------------------------------------------
    #                          SIMPLE STATEMENTS
    # -------------------------------------------------------------------------

    @v.when(AssignmentStatement)
    def visit(self, node):
        ''' AssignmentStatement '''
        pass

    @v.when(PrintStatement)
    def visit(self, node):
        ''' PrintStatement '''
        pass

    @v.when(BreakStatement)
    def visit(self, node):
        ''' BreakStatement '''
        pass

    @v.when(ContinueStatement)
    def visit(self, node):
        ''' ContinueStatement '''
        pass

    # -------------------------------------------------------------------------
    #                           COMPOUND STATEMENTS
    # -------------------------------------------------------------------------

    @v.when(StatementList)
    def visit(self, node):
        ''' StatementList '''
        pass

    @v.when(IfStatement)
    def visit(self, node):
        ''' IfStatement '''
        pass

    @v.when(ForStatement)
    def visit(self, node):
        ''' ForStatement '''
        pass

    # -------------------------------------------------------------------------
    #                              FUNCTIONS
    # -------------------------------------------------------------------------

    @v.when(FunctionCall)
    def visit(self, node):
        ''' FunctionCall '''
        pass

    @v.when(FunctionDefinition)
    def visit(self, node):
        ''' FunctionDefinition '''
        pass

    @v.when(DefParam)
    def visit(self, node):
        ''' DefParam '''
        pass

    @v.when(CallParam)
    def visit(self, node):
        ''' CallParam '''
        pass

    @v.when(ReturnStatement)
    def visit(self, node):
        ''' ReturnStatement '''
        pass
