from ez.models.base import ASTObj, ASTError
from ez.semantic.error import EZSemanticError
from ez.models.types import IntType, PointerType, ArrayType


class ArrayInit(ASTObj):

    '''
    An array initialization AST node.

    array[length] { values }
    '''

    def __init__(self, token, length, values):
        super(ArrayInit, self).__init__(token)

        self.length = length
        self.values = values

        if values is not None and len(values) != length:
            raise EZSemanticError(self,
                                  ArrayInitError('The number of values in' +
                                                 ' the array is' +
                                                 ' not equal to its size.',
                                                 self.length, self.values))

    def __repr__(self):
        if self.values is not None:
            return 'array[{}] = {} '.format(
                self.length,
                self.values)
        else:
            return 'array[{}]'.format(
                self.length)

    def accept_children(self, visitor):
        ''' Tells a visitor to visit the children. '''

        if self.values is not None:
            for value in self.values:
                value.accept(visitor)

    def _compute_type(self, observable):
        '''
        Compute the type of the current node according to the object type of
        the observable.
        '''

        inner_type = observable.obj_type

        if self.values is not None:
            for value in self.values:
                if value.obj_type != inner_type:
                    raise EZSemanticError(self,
                                          ObjTypeError(value.obj_type,
                                                       inner_type))

        self._set_obj_type(PointerType(inner_type,
                                       ArrayType(self.length,
                                                 inner_type)))

    def type_did_updated(self, type_obj):
        ''' Tells the current node that its type is known. '''
        self._set_obj_type(type_obj)


class ArrayAccess(ASTObj):
    '''
    An access to a value at an index of an array.

    array_identifier[index]
    '''
    def __init__(self, token, array_identifier, index):
        super(ArrayAccess, self).__init__(token)

        self.array_identifier = array_identifier
        self.is_assignment = False
        self.index = index
        self.inner_type = None

    def __repr__(self):
        return '{}[{}]'.format(self.array_identifier, self.index)

    def accept_children(self, visitor):
        ''' Tells a visitor to visit the children. '''
        self.array_identifier.accept(visitor)
        self.index.accept(visitor)

    def _compute_type(self, observable):
        '''
        Compute the type of the current node according to the object type of
        the observable.
        '''
        self.check_type(self.index.obj_type, IntType)

        pointer_type = self.array_identifier.obj_type

        self.check_type(pointer_type, PointerType)
        self.check_type(pointer_type.from_type, ArrayType)

        self.array_type = pointer_type.from_type

        if self.is_assignment and self.inner_type is not None:
            self.array_type.set_inner_type(self.inner_type)

        self.array_type.add_inner_type_observer(self)

    def inner_type_did_updated(self, container_type):
        ''' Tells the current node that its inner type is known. '''
        self._set_obj_type(container_type.inner_type)


class ArrayInitError(ASTError):
    '''
    An error raised when there was an error at the initialization of an array.
    '''
    def __init__(self, message, length, values):
        super(ASTError, self).__init__(message)

        self.length = length
        self.values = values
