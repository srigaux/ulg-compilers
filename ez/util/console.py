import code
import atexit
import readline


class HistoryConsole(code.InteractiveConsole):
    '''
    An interactive console that stores the history of the commands to a file.
    '''

    def __init__(self, histfile, locals=None, filename="<console>"):
        code.InteractiveConsole.__init__(self, locals, filename)
        self.init_history(histfile)

    def init_history(self, histfile):
        ''' Initializes the history using the given file. '''
        if hasattr(readline, "read_history_file"):
            try:
                readline.read_history_file(histfile)
            except IOError:
                pass
            atexit.register(self.save_history, histfile)

    def save_history(self, histfile):
        ''' Save the history to a file. '''
        readline.write_history_file(histfile)
