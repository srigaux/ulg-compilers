from base import ASTObj


class FunctionDefinition(ASTObj):
    '''
    A function definition.

    function name(args[0], args[1], ...) {body}
    '''

    def __init__(self, token, name, args, body):
        super(FunctionDefinition, self).__init__(token)

        self.name = name
        self.args = args
        self.body = body
        self.function_type = None

    def __repr__(self):
        return 'function {}({})\n\t{}'.format(self.name, self.args, self.body)

    def accept_children(self, visitor):
        ''' Tells a visitor to visit the children. '''
        for arg in self.args:
            arg.accept(visitor)

        self.body.accept(visitor)

    def _compute_type(self, observable):
        self._set_obj_type(observable.obj_type)


class FunctionCall(ASTObj):
    '''
    A function call.

    name(args)
    '''

    def __init__(self, token, name, arg_values):
        super(FunctionCall, self).__init__(token)

        self.name = name
        self.arg_values = arg_values

    def __repr__(self):
        return '[{}{}]'.format(self.name, self.arg_values)

    def accept_children(self, visitor):
        ''' Tells a visitor to visit the children. '''
        for arg in self.arg_values:
            arg.accept(visitor)

    def _compute_type(self, observable):
        '''
        Compute the type of the current node according to the object type of
        the observable.
        '''
        self._set_obj_type(observable.obj_type)

    def definition_did_updated(self, definition):
        '''
        Tells the current node when the FunctionType knows its
        FunctionDefinition node.
        '''
        self._add_dependency(definition.body)

        # get identifier of each definition argument
        arg_identifiers = definition.args

        for expr, identifier in zip(self.arg_values, arg_identifiers):
            identifier.uid._add_dependency(expr)


class DefParam(ASTObj):
    '''
    A parameter of a function definition.

    description:name
    '''

    def __init__(self, token, description, name):
        super(DefParam, self).__init__(token)

        self.description = description
        self.name = name

    def __repr__(self):
        return '{}:{}'.format(self.description, self.name)


class CallParam(ASTObj):
    '''
    A parameter of a function call.

    description:value
    '''

    def __init__(self, token, description, value):
        super(CallParam, self).__init__(token)

        self.description = description
        self.value = value

    def __repr__(self):
        return '{}:{}'.format(self.description, self.value)


class ReturnStatement(ASTObj):
    '''
    A return statement.
    
    return expression
    '''

    def __init__(self, token, expression=None):
        super(ReturnStatement, self).__init__(token)

        self.expression = expression

    def __repr__(self):
        return 'return {}'.format(self.expression)

    def accept_children(self, visitor):
        ''' Tells a visitor to visit the children. '''
        if self.expression is not None:
            self.expression.accept(visitor)

    def _compute_type(self, observable):
        '''
        Compute the type of the current node according to the object type of
        the observable.
        '''
        self._set_obj_type(self.expression.obj_type)


class UnknownReturnTypeError(Exception):
    ''' An error that is raised when the return type is unknown. '''
    pass


class UnknownArgTypeError(Exception):
    ''' An error that is raised when the type of an argument is unknown. '''
    pass
