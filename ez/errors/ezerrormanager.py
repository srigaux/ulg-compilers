
from ply.lex import LexToken

from ez.models.base import ASTObj
from ez.util.errormanager import ErrorManager
from ez.tokenizer.eztokenizer import EZTokenizerError


class EZErrorManager(ErrorManager):
    ''' Manages the EZ related errors that occured during the compilation. '''

    def __init__(self, content=None, color=False, errors_context_line=3):
        super(EZErrorManager, self).__init__(content, color,
                                             errors_context_line)

    @staticmethod
    def manager(content=None, color=False, errors_context_line=3):
        return EZErrorManager(content=content, color=color,
                              errors_context_line=errors_context_line)

    def _get_range_from_lexpos(self, cstart, cend):
        '''
        Returns the start line, start column, end line and end column from a
        start character position and an end character position (those positions
        are obtained from the lexpos of a token).

        This is used to delimit the portion of code to display.
        '''
        sline, scol = self._find_column(cstart)
        eline, ecol = self._find_column(cend)

        if scol == ecol:
            if len(self.content_lines[eline]) > ecol:
                ecol += 1

        sline += 1
        scol += 1
        eline += 1
        ecol += 1

        return ((sline, scol), (eline, ecol))

    def _get_error_range(self, error):
        '''
        Returns the start line, start column, end line and end column from an
        EZTokenizerError.

        This is used to delimit the portion of code to display.
        '''

        result = super(EZErrorManager, self)._get_error_range(error)

        if result is not None:
            return result

        source = error.source

        if isinstance(source, LexToken):

            if isinstance(error, EZTokenizerError):
                length = 1
            else:
                length = len(str(source.value))

            cstart = source.lexpos
            cend = cstart + length
            return self._get_range_from_lexpos(cstart, cend)

        elif isinstance(source, ASTObj):

            cstart, cend = source.source_range
            return self._get_range_from_lexpos(cstart, cend)

        else:
            raise NotImplementedError(source.__class__)
