import os
import ez

from ez.visitors.scope import ScopeVisitor
from ez.visitors.findreturn import FindReturnVisitor
from ez.visitors.dependencies import DependenciesVisitor
from ez.visitors.dependenciesgraph import DependenciesGraphVisitor
from ez.visitors.uninitialized import UninitializedVisitor
from ez.visitors.printer import PrettyPrintVisitor


class EZSemanticChecker(object):
    ''' Checks the semantic of the AST. '''
    def __init__(self, error_manager):
        super(EZSemanticChecker, self).__init__()

        self.error_manager = error_manager

    @staticmethod
    def checker(error_manager):
        return EZSemanticChecker(error_manager)

    def check(self, ast_tree):
        ''' Checks the semantic of the given AST. '''

        ez_path = os.path.dirname(ez.__file__)
        js_path = os.path.join(ez_path, 'resources/data.js')

        visitors = [
            ScopeVisitor(),
            FindReturnVisitor(),
            DependenciesVisitor(),
            #PrettyPrintVisitor(),
            DependenciesGraphVisitor(js_path),
            UninitializedVisitor(self.error_manager),
        ]

        for visitor in visitors:
            ast_tree.accept(visitor)
