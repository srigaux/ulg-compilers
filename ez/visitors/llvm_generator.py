from ez.models.base import *
from ez.models.arrays import *
from ez.models.compound_statements import *
from ez.models.expressions import *
from ez.models.functions import *
from ez.models.literals import *
from ez.models.simple_statements import *
from ez.models.types import *

from ez.util import visit as v

import llvm

from llvm import *
from llvm.core import Constant, Module, Builder, Function, Type as LlvmType
from llvm.ee import TargetData
from llvm.passes import (PASS_MEM2REG,
                         PASS_INSTCOMBINE,
                         PASS_REASSOCIATE,
                         PASS_GVN,
                         PASS_SIMPLIFYCFG,
                         FunctionPassManager)


class LLVMGeneratorVisitor(object):
    '''
    A visitor that will generate all the LLVM code of the program into a module.
    '''

    def __init__(self, error_manager, optimize=True):
        super(LLVMGeneratorVisitor, self).__init__()

        self.error_manager = error_manager

        self.module = Module.new('main')

        # Frequently used types
        self.int32_t = LlvmType.int()
        self.int8_t = LlvmType.int(8)
        self.int8p_t = LlvmType.pointer(self.int8_t)
        self.zero = Constant.int(self.int32_t, 0)

        # Printf importation
        self.printf_t = LlvmType.function(self.int32_t, [self.int8p_t], True)
        self.printf = self.module.add_function(self.printf_t, 'printf')

        # String global variables to print booleans
        true = Constant.stringz('Yes')
        false = Constant.stringz('No')

        self.true_format = self.module \
            .add_global_variable(true.type,
                                 'YesFormat')
        self.true_format.initializer = true

        self.false_format = self.module \
            .add_global_variable(false.type,
                                 'NoFormat')
        self.false_format.initializer = false

        self.builder = None
        self.global_values = {}
        self.named_values = {}

        self.continue_branch = None
        self.break_branch = None
        self.block_has_termination = False

		# Declares first all the function (without giving the implementation)
        # if they have been used.
        function_definition_nodes = []
        for t in Type.instances.values():
            if isinstance(t, FunctionType):
                function_definition_nodes.append(t._definition)

        for node in function_definition_nodes:
            self._declare_function(node)

        self.pass_manager = FunctionPassManager.new(self.module)

        # executor = ExecutionEngine.new(module)

        self.optimize = optimize

        if optimize:
            #Set up the optimizer pipeline. Start with registering info about
            #how the target lays out data structures.
            self.pass_manager.add(TargetData.new(''))
            #Promote allocas to registers.
            self.pass_manager.add(PASS_MEM2REG)
            #Do simple "peephole" optimizations and bit-twiddling optzns.
            self.pass_manager.add(PASS_INSTCOMBINE)
            #Reassociate expressions.
            self.pass_manager.add(PASS_REASSOCIATE)
            #Eliminate Common SubExpressions.
            self.pass_manager.add(PASS_GVN)
            #Simplify the control flow graph (deleting unreachable blocks, etc).
            self.pass_manager.add(PASS_SIMPLIFYCFG)

        self.pass_manager.initialize()

    @staticmethod
    def CreateEntryBlockAlloca(function, var_name, var_type):
        '''
        Creates an alloca instruction in the entry block of the function.
        This is used for mutable variables.
        '''
        entry = function.get_entry_basic_block()
        builder = Builder.new(entry)
        builder.position_at_beginning(entry)
        return builder.alloca(var_type, name=var_name)

    @v.on('node')
    def visit(self, node):
        '''This is the generic method that initializes the dynamic dispatcher.
        '''

    # -------------------------------------------------------------------------
    #                              BASE
    # -------------------------------------------------------------------------

    @v.when(ASTObj)
    def visit(self, node):
        ''' ASTObj '''
        raise NotImplementedError(node)

    @v.when(Program)
    def visit(self, node):
        ''' Program '''
        from ez.models.functions import FunctionDefinition

        # Generate the functions of the program
        result = []
        for statement in node.statements:
            if isinstance(statement, FunctionDefinition):
                result.append(self.visit(statement))

        return result

    @v.when(Identifier)
    def visit(self, node):
        ''' Identifier '''

        # Declare the variables if they have not been previously declared
        if node.scoped_name() not in self.named_values:

            # Declare only if it is a global variable
            if node.scope_name is None:  # global_variables
                global_var = self.module \
                    .add_global_variable(node.obj_type.llvm_type(),
                                         node.scoped_name())

                global_var.initializer = \
                    Constant.null(node.obj_type.llvm_type())

                self.global_values[node.scoped_name()] = global_var
                self.named_values[node.scoped_name()] = global_var

            else:
                raise RuntimeError('Unknown variable name: '
                                   + node.scoped_name())

        # Load the variable
        return self.builder.load(self.named_values[node.scoped_name()],
                                 node.scoped_name())

    # -------------------------------------------------------------------------
    #                              LITERALS
    # -------------------------------------------------------------------------

    @v.when(Integer)
    def visit(self, node):
        ''' Integer '''
        return Constant.int(node.obj_type.llvm_type(), node.value)

    @v.when(Float)
    def visit(self, node):
        ''' Float '''
        return Constant.real(node.obj_type.llvm_type(), node.value)

    @v.when(Boolean)
    def visit(self, node):
        ''' Boolean '''
        return Constant.int(LlvmType.int(1), node.value)

    @v.when(Void)
    def visit(self, node):
        ''' Void '''

    # -------------------------------------------------------------------------
    #                              ARRAYS
    # -------------------------------------------------------------------------

    @v.when(ArrayInit)
    def visit(self, node):
        ''' ArrayInit '''
        # Get the inner type
        inner_type = node.obj_type.inner_type
        #inner_llvm_type = inner_type.llvm_type()

        array_type = ArrayType(node.length,
                               inner_type)
        array_llvm_type = array_type.llvm_type()

        function = self.builder.basic_block.function

        # Allocate the array in the current function
        allocarray = \
            self.CreateEntryBlockAlloca(function,
                                        'array',
                                        array_llvm_type)


        # Get a pointer on the first item.
        ptr = self.builder.gep(allocarray,
                               [self.zero, self.zero],
                               'array_ptr')
        iterator = ptr

        # For each item, initialize the value or set it to zero
        for i in range(node.length):

            if node.values is not None:
                val_code = self.visit(node.values[i])
            else:
                val_code = Constant.null(inner_type.llvm_type())

            self.builder.store(val_code, iterator)
            # Go to the next item
            iterator = self.builder.gep(iterator,
                                        [Constant.int(LlvmType.int(), 1)],
                                        'it_ptr')

        return ptr

    @v.when(ArrayAccess)
    def visit(self, node):
        ''' ArrayAccess '''
        # Compute the index
        index_value = self.visit(node.index)

        # Get the pointer of the pointer of the first item of the array
        array_ptr_ptr = self.named_values[node.array_identifier.scoped_name()]

        # Load the pointer of the first item
        array_ptr = self.builder.load(array_ptr_ptr, 'array_ptr')

        # Move to the given index
        ptr = self.builder.gep(array_ptr,
                               [index_value])

        # Return the pointer or load the data
        if node.is_assignment:
            return ptr
        else:
            return self.builder.load(ptr, 'array_access')

    # -------------------------------------------------------------------------
    #                              EXPRESSIONS
    # -------------------------------------------------------------------------

    @v.when(Power)
    def visit(self, node):
        ''' Power '''
        base = self.visit(node.base)
        exponent = self.visit(node.exponent)

        base = NumericType.cast(base, node.base.obj_type, FloatType)
        exponent = NumericType.cast(
            exponent, node.exponent.obj_type, FloatType)

        t_float = LlvmType.float()
        f_pow = Function.intrinsic(self.module, llvm.core.INTR_POW, [t_float])

        return self.builder.call(f_pow, [base, exponent], 'pow_call')

    @v.when(UnaryExpr)
    def visit(self, node):
        ''' UnaryExpr '''
        value = self.visit(node.value)

        if node.value.obj_type == IntType():
            return self.builder.neg(value)
        else:
            return self.builder.fsub(Constant.real(node.value.obj_type.llvm_type(), 0),
                                     value,
                                     'neg')

    @v.when(BinaryExpr)
    def visit(self, node):
        ''' BinaryExpr '''
        a = self.visit(node.a)
        b = self.visit(node.b)

        # Returns float if there is one of the operand is a float otherwise int
        result_type = NumericType.get_bigest_type([node.a.obj_type,
                                                   node.b.obj_type])

        a = NumericType.cast(a, node.a.obj_type, result_type)
        b = NumericType.cast(b, node.b.obj_type, result_type)

        if result_type == IntType:
            if node.operator == '+':
                return self.builder.add(a, b, 'addtmp')
            elif node.operator == '-':
                return self.builder.sub(a, b, 'subtmp')
            elif node.operator == '*':
                return self.builder.mul(a, b, 'multmp')
            elif node.operator == '/':
                return self.builder.sdiv(a, b, 'divtmp')
            elif node.operator == '//':
                raise NotImplementedError()
            elif node.operator == '%':
                return self.builder.srem(a, b, 'modtemp')

            else:
                raise RuntimeError('Unknown binary operator.')

        elif result_type == FloatType:
            if node.operator == '+':
                return self.builder.fadd(a, b, 'faddtmp')
            elif node.operator == '-':
                return self.builder.fsub(a, b, 'fsubtmp')
            elif node.operator == '*':
                return self.builder.fmul(a, b, 'fmultmp')
            elif node.operator == '/':
                return self.builder.fdiv(a, b, 'fdivtmp')
            elif node.operator == '//':
                raise NotImplementedError()
            elif node.operator == '%':
                return self.builder.frem(a, b, 'fmodtemp')

            else:
                raise RuntimeError('Unknown binary operator.')

    @v.when(Comparison)
    def visit(self, node):
        ''' Comparison '''
        codes_operands = []
        # Generate code for all operands
        for operand in node.operands:
            codes_operands.append(self.visit(operand))

        results = []

        # Comute the results of all comparisons two by two
        for i in range(len(node.operands) - 1):

            a = codes_operands[i]
            b = codes_operands[i + 1]

            operand_a = node.operands[i]
            operand_b = node.operands[i + 1]

            biggest_type = NumericType.get_bigest_type([operand_a.obj_type,
                                                        operand_b.obj_type])

            a = NumericType.cast(a, operand_a.obj_type, biggest_type)
            b = NumericType.cast(b, operand_b.obj_type, biggest_type)

            operator = self._operator_to_llvm(biggest_type, node.operators[i])

            if biggest_type == IntType:
                results.append(
                    self.builder.icmp(operator, a, b, name='int_cmp')
                )
            elif biggest_type == FloatType:
                results.append(
                    self.builder.fcmp(operator, a, b, name='float_cmp')
                )
            else:
                raise ObjTypeError('Unknown type \'{}\''.format(biggest_type))

        # Compute the final result of the comparison(s)
        prev_result = None

        for i in range(len(results)):

            if len(results) == 1:
                prev_result = results[i]
            elif prev_result == None:
                prev_result = self.builder.and_(results[i], results[i + 1])
            else:
                prev_result = self.builder.and_(prev_result, results[i])

        return prev_result

    def _operator_to_llvm(self, our_type, operator):
        '''
        Returns the LLVM comparison constant according to the given type and
        the given operator
        '''

        # Integer comparisons
        if our_type == IntType:
            if operator == '==':
                return llvm.core.ICMP_EQ

            elif operator == '!=' or operator == '<>':
                return llvm.core.ICMP_NE

            elif operator == '>':
                return llvm.core.ICMP_SGT

            elif operator == '>=' or operator == '=>':
                return llvm.core.ICMP_SGE

            elif operator == '<':
                return llvm.core.ICMP_SLT

            elif operator == '<=' or operator == '=<':
                return llvm.core.ICMP_SLE

        # Float comparisons
        elif our_type == FloatType:
            if operator == '==':
                return llvm.core.FCMP_UEQ

            elif operator == '!=' or operator == '<>':
                return llvm.core.FCMP_UNE

            elif operator == '>':
                return llvm.core.FCMP_UGT

            elif operator == '>=' or operator == '=>':
                return llvm.core.FCMP_UGE

            elif operator == '<':
                return llvm.core.FCMP_ULT

            elif operator == '<=' or operator == '=<':
                return llvm.core.FCMP_ULE

        else:
            raise TypeError('Unknown type \'{}\''.format(our_type))

    @v.when(IfElseExpr)
    def visit(self, node):
        ''' IfElseExpr '''
        b = self.visit(node.b)

        function = self.builder.basic_block.function

        # Create blocks for the then and else cases.
        # Insert the 'then' block at the
        # end of the function.
        then_block = function.append_basic_block('then')
        else_block = function.append_basic_block('else')
        merge_block = function.append_basic_block('ifcond')

        self.builder.cbranch(b, then_block, else_block)

        self.builder.position_at_end(then_block)
        then_value = self.visit(node.a)
        self.builder.branch(merge_block)

        # code_gen of 'Then' can change the current block;
        # update then_block for the PHI node.
        then_block = self.builder.basic_block

        # Emit else block.
        self.builder.position_at_end(else_block)
        else_value = self.visit(node.c)
        self.builder.branch(merge_block)

        # code_gen of 'Else' can change the current block,
        # update else_block for the PHI node.
        else_block = self.builder.basic_block

        # Emit merge block.
        self.builder.position_at_end(merge_block)
        phi = self.builder.phi(node.a.obj_type.llvm_type(), 'iftmp')
        phi.add_incoming(then_value, then_block)
        phi.add_incoming(else_value, else_block)

        return phi

    @v.when(OrTest)
    def visit(self, node):
        ''' OrTest '''
        a = self.visit(node.a)
        b = self.visit(node.b)

        return self.builder.or_(a, b, name='or_test')

    @v.when(AndTest)
    def visit(self, node):
        ''' AndTest '''
        a = self.visit(node.a)
        b = self.visit(node.b)

        return self.builder.and_(a, b, name='or_test')

    @v.when(NotTest)
    def visit(self, node):
        ''' NotTest '''
        a = self.visit(node.a)

        return self.builder.not_(a, name='not_test')

    # -------------------------------------------------------------------------
    #                          SIMPLE STATEMENTS
    # -------------------------------------------------------------------------

    @v.when(AssignmentStatement)
    def visit(self, node):
        ''' AssignmentStatement '''
        function = self.builder.basic_block.function

        value = self.visit(node.value)

        for target in node.targets:

            if isinstance(target, Identifier):
                if target.scoped_name() in self.named_values:
                    # Look up the name.
                    variable = self.named_values[target.scoped_name()]

                    # Store the value and return it.
                    self.builder.store(value, variable)

                else:

                    if target.scope_name is None:  # global variable
                        global_var = self.module \
                            .add_global_variable(target.obj_type.llvm_type(),
                                                 target.scoped_name())

                        global_var.initializer = \
                            Constant.null(target.obj_type.llvm_type())

                        self.global_values[target.scoped_name()] = global_var

                        var = global_var

                    else:
                        var = \
                            self.CreateEntryBlockAlloca(function,
                                                        target.scoped_name(),
                                                        node.value.obj_type.llvm_type())

                    self.builder.store(value, var)

                    # Remember the old variable binding so that we can restore
                    # the binding when we unrecurse.
                    #old_bindings[var_name] = g_named_values.get(var_name, None)

                    # Remember this binding.
                    self.named_values[target.scoped_name()] = var

            elif isinstance(target, ArrayAccess):

                assert(
                    target.array_identifier.scoped_name() in self.named_values)

                ptr = self.visit(target)
                self.builder.store(value, ptr)

            else:
                assert(False)  # unexpected target of assignement
        return value

    @v.when(PrintStatement)
    def visit(self, node):
        ''' PrintStatement '''

        #comute the format string for the print
        format = ''
        for expr in node.expressions:
            format += self._get_print_format(expr.obj_type)

        format += '\n'

        function = self.builder.basic_block.function

        #Create un global variable for the print format

        format_data = Constant.stringz(format)
        format_array = self.module.add_global_variable(format_data.type,
                                                       'format')
        format_array.initializer = format_data
        format_ptr = format_array.gep([self.zero, self.zero])

        args = [format_ptr]

        for expr in node.expressions:

            if isinstance(expr.obj_type, VoidType):
                continue

            elif isinstance(expr.obj_type, BooleanType):

                expr_code = self.visit(expr)

                true_ptr = self.builder.gep(self.true_format,
                                            [self.zero, self.zero])

                false_ptr = self.builder.gep(self.false_format,
                                             [self.zero, self.zero])

                #Choose between the two pointers to the Yes or No strings
                value = self.builder.select(expr_code,
                                            true_ptr,
                                            false_ptr,
                                            'boolformat')
                args.append(value)

            elif isinstance(expr.obj_type, PointerType):

                expr_code = self.visit(expr)

                size = expr.obj_type.from_type.size

                for i in range(size):
                    ptr = self.builder.gep(expr_code,
                                           [Constant.int(LlvmType.int(),
                                                         i)])

                    value = self.builder.load(ptr, 'val_ptr')
                    args.append(value)

            else:
                value = self.visit(expr)
                args.append(value)

        self.builder.call(self.printf,
                          args,
                          'printf')


    def _get_print_format(self, obj_type):

        if isinstance(obj_type, VoidType):
            return 'void '

        elif isinstance(obj_type, BooleanType):
            return '%s '

        elif isinstance(obj_type, IntType):
            return '%d '

        elif isinstance(obj_type, FloatType):
            return '%f '

        elif isinstance(obj_type, PointerType):

            size = obj_type.from_type.size

            return \
                '[' + \
                (self._get_print_format(obj_type.inner_type) * size)[:-1] \
                + '] '

    @v.when(BreakStatement)
    def visit(self, node):
        ''' BreakStatement '''

        if self.break_branch is None:
            raise LLVMException('Break not in a loop')

        self.block_has_termination = True
        self.builder.branch(self.break_branch)

    @v.when(ContinueStatement)
    def visit(self, node):
        ''' ContinueStatement '''

        if self.continue_branch is None:
            raise LLVMException('Continue not in a loop')

        self.block_has_termination = True
        self.builder.branch(self.continue_branch)

    # -------------------------------------------------------------------------
    #                           COMPOUND STATEMENTS
    # -------------------------------------------------------------------------

    @v.when(StatementList)
    def visit(self, node):
        ''' StatementList '''

        self.block_has_termination = False

        stmts = [self.visit(statement) for statement in node.statements]

        return stmts

    @v.when(IfStatement)
    def visit(self, node):
        ''' IfStatement '''
        a = self.visit(node.a)

        function = self.builder.basic_block.function

        # Create blocks for the then and else cases.
        # Insert the 'then' block at the
        # end of the function.
        then_block = function.append_basic_block('then')
        else_block = function.append_basic_block('else')
        after_block = function.append_basic_block('after_if')

        self.builder.cbranch(a, then_block, else_block)

        self.builder.position_at_end(then_block)
        self.visit(node.b)

        if not self.block_has_termination:
            self.builder.branch(after_block)

        self.block_has_termination = False

        # code_gen of 'Then' can change the current block;
        # update then_block for the PHI node.
        then_block = self.builder.basic_block

        # Emit else block
        self.builder.position_at_end(else_block)
        if node.c is not None:
            self.visit(node.c)

        if node.c is None or not self.block_has_termination:
            self.builder.branch(after_block)

        self.block_has_termination = False
        # code_gen of 'Else' can change the current block,
        # update else_block for the PHI node.
        else_block = self.builder.basic_block

        # Any new code will be inserted in after_block.
        self.builder.position_at_end(after_block)

    @v.when(ForStatement)
    def visit(self, node):
        ''' ForStatement '''
        function = self.builder.basic_block.function

        self.visit(node.init)

        # Make the new basic block for the loop, inserting after current block.
        loop_block = function.append_basic_block('loop')
        increment = function.append_basic_block('increment')
        after_block = function.append_basic_block('afterloop')

        continue_branch_bak = self.continue_branch
        break_branch_bak = self.break_branch
        block_has_termination_bak = self.block_has_termination

        self.continue_branch = increment
        self.break_branch = after_block
        self.block_has_termination = False

        # Insert an explicit fallthrough from the
        # current block to the loop_block.
        self.builder.branch(loop_block)

        # Start insertion in loop_block.
        self.builder.position_at_end(loop_block)

        # Emit the body of the loop.  This, like any other expr, can change the
        # current BB.  Note that we ignore the value computed by the body.
        self.visit(node.body)

        #loop_block = self.builder.basic_block

        if not self.block_has_termination:
            self.builder.branch(increment)

        self.continue_branch = continue_branch_bak
        self.break_branch = break_branch_bak
        self.block_has_termination = block_has_termination_bak

        self.builder.position_at_end(increment)

        # Compute the end condition
        end_condition_bool = self.visit(node.cond)

        self.visit(node.increment)

        ## Create the "after loop" block and insert it.
        #after_block = function.append_basic_block('afterloop')

        # Insert the conditional branch into the end of loop_end_block.
        self.builder.cbranch(end_condition_bool, loop_block, after_block)

        # Any new code will be inserted in after_block.
        self.builder.position_at_end(after_block)

        # for expr always returns 0.0.
        # return Constant.void()

    # -------------------------------------------------------------------------
    #                              FUNCTIONS
    # -------------------------------------------------------------------------

    def _declare_function(self, function_definition_node):

        node = function_definition_node

        # Get the return type
        if not node.function_type.is_used:
            self.error_manager.add_error(
                EZSemanticError(function_definition_node,
                                'Function not used',
                                EZSemanticError.NOTICE))
            return

        return_type = node.obj_type.llvm_type()

        # Get the arguments types
        args_types = []
        for arg_type in [arg_identifier.obj_type
                         for arg_identifier
                         in node.args]:

            if arg_type is None:
                raise UnknownArgTypeError('The type of argument \'{}\' of '
                                          'function \'{}\' is not known.'
                                          .format(arg_type,
                                                  node.name))

            if isinstance(arg_type, ArrayType):
                ptr_type = LlvmType.pointer(arg_type.llvm_type())
                args_types.append(ptr_type)
            else:
                args_types.append(arg_type.llvm_type())

        # Build the function and its type
        funct_type = LlvmType.function(
            return_type, args_types, False)

        return Function.new(self.module, funct_type, node.name)

    @v.when(FunctionDefinition)
    def visit(self, node):
        ''' FunctionDefinition '''

        if not node.function_type.is_used:
            return

        self.named_values = self.global_values.copy()

        function = self.module.get_function_named(node.name)

        block = function.append_basic_block('entry')
        self.builder = Builder.new(block)

        # Allocate variables for the arguments and store values in them
        for arg_identifier, arg in zip(node.args, function.args):
            alloca = self.CreateEntryBlockAlloca(function,
                                                 arg_identifier.scoped_name(),
                                                 arg.type)
            self.builder.store(arg, alloca)
            self.named_values[arg_identifier.scoped_name()] = alloca

        try:
            self.visit(node.body)

            # Build the return statement
            if not node.body.has_return:

                if isinstance(node.body.obj_type, VoidType):
                    self.builder.ret_void()
                else:
                    self.builder.ret(self.zero)

            function.verify()

            # Optimize the function.
            self.pass_manager.run(function)

        except:
            function.delete()
            raise

        return function

    @v.when(FunctionCall)
    def visit(self, node):
        ''' FunctionCall '''
        # Look up the name in the global module table.
        callee = self.module.get_function_named(node.name)

        # Check for argument mismatch error.
        if len(callee.args) != len(node.arg_values):
            raise RuntimeError('Incorrect number of arguments passed.')

        arg_values = []

        for arg in node.arg_values:

            # Get the pointer of the array passed as parameter
            if isinstance(arg, ArrayType):
                array = self.visit(arg)
                ptr = self.builder.gep(array,
                                       [self.zero, self.zero],
                                       'array_ptr')
                arg_values.append(ptr)

            else:
                arg_values.append(self.visit(arg))

        # Don't name the call if the return type is void
        if isinstance(node.obj_type, VoidType):
            return self.builder.call(callee, arg_values)
        else:
            return self.builder.call(callee, arg_values, 'calltmp')

    @v.when(DefParam)
    def visit(self, node):
        ''' DefParam '''
        raise NotImplementedError()

    @v.when(CallParam)
    def visit(self, node):
        ''' CallParam '''
        raise NotImplementedError()

    @v.when(ReturnStatement)
    def visit(self, node):
        ''' ReturnStatement '''

        self.block_has_termination = True

        if isinstance(node.expression, Void):
            self.builder.ret_void()
        else:
            value = self.visit(node.expression)
            return self.builder.ret(value)
